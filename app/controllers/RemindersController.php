<?php
/*************************************************
*	*Reminders Controller
*	*Functionality :
*		- getRemind() : Render Reminder View 
		- postRemind() :   Get email address from reminder view to verify in the user table 
						   email valid or invaid
		- getReset() :  Get token to reset password	
		- postReset() : Get New password and Confirm password to set New password
*	*Developed & designed By : ABI-Technologies
*	*History :
*	
*************************************************/
class RemindersController extends Controller {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		return View::make('password.remind');
	}
	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		/*switch ($response = Password::remind(Input::only('email')))*/
		Config::set('auth.reminder.email', 'emails.auth.reminder');
		switch ($response = Password::users()->remind(Input::only('email'), function($message){
            $message->subject('CRM - Password Reset');
        }))
        {
			case Password::INVALID_USER:
				return Redirect::back()->with('error', Lang::get($response));

			case Password::REMINDER_SENT:
				return Redirect::back()->with('status', Lang::get($response));
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($type = null,$token = null)
	{
		if (is_null($token)) App::abort(404);

		return View::make('password.reset')->with('token', $token)->with('type',$type);
	}
	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		$credentials = Input::only(
			'email', 'password', 'password_confirmation', 'token'
		);
		/*
		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = Hash::make($password);

			$user->save();
		});*/
		$response = Password::users()->reset($credentials, function($agent, $password) {
    			$agent->password = Hash::make($password);
    			$agent->save();
		});
		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				return Redirect::back()->with('error', Lang::get($response));

			case Password::PASSWORD_RESET:
				return Redirect::action('AgentController@getLogin');
		}
	}

}
