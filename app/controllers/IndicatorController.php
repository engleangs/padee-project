<?php 
/*************************************************
*	*File Name: Indicator Controller
*	*Functionality: 
	*History:

		- 2015-06-01 Sim Chhayrambo Initial Version

*	*Developed & designed By : ABI-Technologies
*	
*************************************************/
class IndicatorController extends \BaseController
{
	protected  $indexView = 'indicator.index';
 	protected  $editView  = 'indicator.edit';
 	protected  $addView   = 'indicator.add';
 	protected  $viewView  = 'indicator.view';
 	protected  $modalView = 'indicator.modal';
 	protected  $modelName = "Indicator";
 	protected  $module 	 = 'indicator';
 	protected  $can_add   ='can_add';
	protected  $can_view  ='can_view';
	protected  $can_edit  = 'can_edit';
	protected  $can_delete='can_delete';
 	protected  $defaultBackUrl = "IndicatorController@anyIndex";
 	protected  $tageName = "admin_agent";
 	protected  $pageView  = 'nopermission.index';
 	protected  $sortFields = [
	 						'title'=>'admin_default.title',
	 					];

 	public function beforeDelete($id)
	{
		$model = $this->model();
		$obj   =	$model->find($id);
		if(!$obj) throw new Exception(trans('admin_default.item_not_found'), 404);

		$this->doRemoveImages($obj->featured_image,public_path().'/resources/agent', $this->resize['featured_image']);
	
	}

 	function getBackUrl($id=0)
 	{
 		if(!$id)
 		{
 			return "IndicatorController@getAdd";
 		}
 		else{
 			return "IndicatorController@getEdit";
 		}
 	}

 	public function getAdd()
 	{
 		if($this->getCheckPermission($this->module,$this->can_add))
 		{
	 		return \View::make($this->addView);
	 	}
	 	else
	 	{
	 		return \View::make($this->pageView);
	 	}
 	}
 	public function getView($id=0)
 	{
 		if($this->getCheckPermission($this->module,$this->can_view))
 		{
	 		$view = parent::getView($id);
	 		$data = $view->getData('item');
	 		return $view;
	 	}
	 	else
	 	{
	 		return \View::make($this->pageView);
	 	}
 	}
 	public function getEdit($id=0)
 	{
 		if($this->getCheckPermission($this->module,$this->can_edit))
 		{
	 		return parent::getEdit($id);
		}
		else
		{	
			return \View::make($this->pageView);
		}
	}
 	public function anyIndex()
 	{
 		if($this->getListPermission())
 		{
		 	$status = Input::get('state',null);
			if(!is_null($status))
			{ 
				$this->setState('search.state',$status);
			}
			$stateStatus=$this->state('search.state',null);
			if($stateStatus!=='')
			{
				$this->condition['status'] = $stateStatus;
			}
	 		$view = parent::anyIndex();
	 		return $view;
		}
		else
		{
			return \View::make($this->pageView);
		}	
 	}
 	public  function postSave($id=0)
 	{
 		try 
	  	{
	  		$this->key = $id;
			$data 	= Input::get($this->tageName, array());

			$model = $this->model();
			$this->key = $model->saveAll($id,$data);
			
			$actionUrl = $this->defaultBackUrl;
     		$message['message'] = trans('admin_default.save_success');
			$message['success'] = true;
			
		} catch (\Exception $e) 
		{
				$message['message'] = $e->getMessage();
				$actionUrl =  $this->getBackUrl($id);
		}

		\Session::flash('message',$message);
		return \Redirect::action($actionUrl,array('id'=>$id));

 	}
 	public function getDelete($id)
 	{
 		if($this->getCheckPermission($this->module,$this->can_delete))
		{
			$message = array(
						'success'=>true,
						'message'=>trans($this->tageName.'.delete_success')
						);
			try 
			{
				$model = $this->model();
 				$obj   =	$model->find($id);
 				if(!$obj) throw new \Exception(trans('admin_default.item_not_found'), 404);
 				\DB::table('indicators')
 					 ->where('id','=',$id)
 					 ->update(['is_deleted'=> 1]);
			} catch (Exception $e) 
			{
				$message['success'] = false;
				$message['message'] = $e->getMessage();
			}
			\Session::flash('message', $message);		

			return \Redirect::action($this->defaultBackUrl);
		}
		else
		{
			return \View::make($this->pageView);
		}
 	}
 	public function postDeleteList()
	{
		return \View::make($this->pageView);
	}
}

?>
