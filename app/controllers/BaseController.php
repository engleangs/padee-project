<?php
/**
 * Base Controller
 * @author Sim Chhayrambo
 * @package controllers
 * @functionlity : 
 * + buildShortName : return encryption class name for state of controller
 * + setState 		: set the state of controller based on key and value
 * + state 			: get state of controller based on key
 * + states 		: get all state of controller
 * @develop&designed: Abi Technologies
 * History 			:
*/
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	protected $pageView  = 'administrator::nopermission.index';
	protected $indexView = '';
	protected $modalView = '';
	protected $editView = '';
	protected $addView	= '';
	protected $viewView = '';
	protected $shortName = '';
	protected $module    ='';
	protected $can_add   ='';
	protected $can_edit  ='';
	protected $can_view  ='';
	protected $can_delete='';
	protected $allStates = array();
	protected $prefix    = '';
	protected $lstModel 	= array();
	protected $modelName = null;
	protected $tageName  = null;
	protected $defaultBackUrl;
	protected $backUrl;
	protected  $key = 0;
	protected  $condition = array();
	protected  $sortFields = array();
	protected  $sortDir 	=[];
	protected  $langs 	= null;
	protected  $resizes = [
						];
	function __construct()
	{
		$this->sortDir['ASC'] = trans('admin_default.asc');
		$this->sortDir['DESC']= trans('admin_default.desc');
	}
	
	function langs()
 	{
 		if(is_null($this->langs))
 		{
 			$langs 	= \Helper::getLangId();
 			$this->langs = array();
 			$this->langs[0] = trans('admin_default.all');
	 		foreach ($langs as $key => $obj) 
	 		{
	 			$this->langs[$key] = $obj->name;
	 		}	
 		}
 		return $this->langs;
 	}
 	public function getCheckPermission($module,$action)
 	{
 		/*return true;*/
 		
 		$permission = \Helper::checkPagePermission($module,$action);
 	    $check = false;
 		if($permission)
 			$check = true;
 		return $check;
 	}
 	public function getListPermission()
 	{
 		/*return true;*/
 		$user_permission = Session::get('user_permission',array());
 		$is_super_admin = Session::get('is_super_admin');
 		$display = false;
 		if ((isset($user_permission[$this->module]['can_view']) && $user_permission[$this->module]['can_view']) || $is_super_admin)
 		{
 			$display =true;
 		}
 		elseif ((isset($user_permission[$this->module]['can_edit']) && $user_permission[$this->module]['can_edit']) || $is_super_admin) {
 			$display = true;
 		}
 		return $display;
 	}
 	public function setCondition()
 	{
 		$filter_lang 	= \Input::get('search_lang_filter',null);
 		foreach ($this->sortFields as $key => $value) 
 		{
 			$this->sortFields[$key] = trans($value);
 		}
 		if(!is_null($filter_lang))
 		{
 			$this->setState('search_lang_filter',(int)$filter_lang); 		
 		}
 		
 		if($search_lang_filter = $this->state('search_lang_filter',null))
 		{
 			$this->condition['search_lang_filter'] = $search_lang_filter;
 		}
 		$search 	= \Input::get('search',null);
 		if(!is_null( $search))
 		{

 			$this->setState('search',$search);
 		}
 		if($search_filter = $this->state('search',null))
 		{
 			$this->condition['search'] = $search_filter;
 		}
 		$sortField = \Input::get('sortField', null);
 		if(!is_null($sortField) && isset($this->sortFields[$sortField]))
 		{
 			$this->setState('sortField',$sortField);
 		}
 		$sortDir = \Input::get('sortDir', null);
 		if(!is_null($sortDir))
 		{
 			$sortDir = strtoupper($sortDir);
 			if($sortDir=="ASC" || $sortDir == "DESC")
 			{
 				$this->setState('sortDir',$sortDir);
 			}
 		}
 		return $this->condition;
 	}
	public function model($modelName=null)
	{
		if(!$modelName)
		{
			if(!$this->modelName) throw new Exception("Could not found default name of Model");
			$modelName = $this->modelName;
			
		}
		if(!isset($this->lstModel[$modelName]))
		{
			if(class_exists($modelName))
			{
				$this->lstModel[$modelName] 	= new $modelName();	
			}
			else throw new Exception("Model Name :".$modelName." not found !", 1);			
			
		}
		return $this->lstModel[$modelName];		

	}

	/**
	 * function BuildShortName to generate 
	 * encryption name of controller
	 * @author Sim Chhayrambo
	 * @return string  (short name of controller)
	 * *
	*/
	protected function buildShortName()
	{
		if(!$this->shortName){
			/*echo get_class($this);
			echo "<br/>";*/
			$this->shortName  = md5(get_class($this).$this->prefix);	
		}
		return $this->shortName;
	}

	/**
	 * Method set state 
	 * for setting state and store on session
	 * base on controller name
	 * and getting by controller
	 * @author Engleang
	 * @param string name
	 * @param any value
	 * 
	*/
	public function setState($name,$value)
	{
		$this->shortName = $this->buildShortName();
		$this->allStates = Session::get($this->shortName, array());
		$this->allStates[$name] = $value;
		Session::set($this->shortName, $this->allStates); 

	}

	/**
	 * Method get state by key
	 * and default value
	 * getting from session based on controller short name
	 * @author 	Engleang
	 * @param 	string 		name
	 * @param 	any  		default
	 * @return 	any 		default
	 * 
	 * 
	**/

	public function state($name,$default=null)
	{
		$this->shortName = $this->buildShortName();
		$this->allStates = Session::get($this->shortName, array());
		if(isset($this->allStates[$name]))
		{
			return $this->allStates[$name];
		}
		
		return $default;
	}

	/**
	 * Method to get all states of controller
	 * @author 	Sim Chhayrambo
	 * @return 	arrray 	allstates
	*/
	public function states()
	{

		$this->shortName = $this->buildShortName();		
		$this->allStates = Session::get($this->shortName, array());
		return $this->allStates;
	}
	public function clearStates()
	{
		$this->shortName = $this->buildShortName();
		Session::set($this->shortName,array());
	}
	/**
	 * Default action modal
	 * 
	*/
	public function anyModal()
	{
		$this->setCondition();
		$per_page = \Input::get('per_page', null);
		if($per_page)
 		{
 			$this->setState('per_page',$per_page);
 		}
 		$per_page = $this->state('per_page',10);
 		$this->setStatePage($per_page);
		$states = $this->states(); 		
 		$modelName = $this->modelName; 		
 		if(!$modelName) throw new Exception("No default model name found !", 1); 		
 		$model 	= $this->model();
 		$per_page = $this->state('per_page',10); 		
 		$order 	= $this->state('sortField','created_at');
 		$dir 	= $this->state('sortDir','DESC');
 		if(!isset($this->sortFields[$order]))
 		{
 			$order = null;
 		}

		$data = $model->doPaginate($per_page,$this->condition,$order,$dir);
		$requestPage 	=  \Input::get('page',1);
		if(!count($data) &&  $requestPage>$data->getLastPage())
		{
			\Input::merge(array('page'=>$data->getLastPage()));
			$data = $model->doPaginate($per_page,$this->condition,$order,$dir);
		}		
		if(!$this->indexView) throw new Exception("Index view not yet set in this controller", 1);		
		
		return \View::make($this->modalView,array(
								'data'=>$data,
								'states'=>$states,
								'sortFields'=>$this->sortFields,
								'sortDir'=>$this->sortDir 
								)
					);  

	}

	protected function setStatePage($per_page)
	{
		$condition = json_encode( $this->condition );
		$condition = $condition.$per_page;
		$prefix = md5($condition);
		$page 	= Input::get('page',null);
 		if(is_null($page))
 		{
 			$page = $this->state($prefix.'page',1);
 			
 			
 			if($page>1)
 			{
 				Input::merge(array('page'=>$page));
 			}
 		}
 		else
 		{
 			$this->setState($prefix.'page',$page);
 		}
 		$states = $this->states();
 		

 	

	}
	/**
	 * Default action any index
	 * @author Engleang
	 * @param  Input : per_page
	 * @param  Input : search
	 * @return View with data
	 * 
	*/

	public function anyIndex()
	{
		if($this->getListPermission())
		{
			$this->setCondition();
			$per_page = \Input::get('per_page', null);
			if($per_page)
	 		{
	 			$this->setState('per_page',$per_page);
	 		}
	 			 		
	 		$per_page = $this->state('per_page',10); 
	 		$this->setStatePage($per_page);
			$states = $this->states(); 		
	 		$modelName = $this->modelName; 		
	 		if(!$modelName) throw new Exception("No default model name found !", 1); 		
	 		$model 	= $this->model();
	 				
	 		$order 	= $this->state('sortField','created_at');
	 		$dir 	= $this->state('sortDir','DESC');
	 		if(!isset($this->sortFields[$order]))
	 		{
	 			$order = null;
	 		}

			$data = $model->doPaginate($per_page,$this->condition,$order,$dir);
			$requestPage 	=  \Input::get('page',1);
			if(!count($data) &&  $requestPage>$data->getLastPage())
			{
				\Input::merge(array('page'=>$data->getLastPage()));
				$data = $model->doPaginate($per_page,$this->condition,$order,$dir);
			}			
			if(!$this->indexView) throw new Exception("Index view not yet set in this controller", 1);		
			return \View::make($this->indexView,array(
									'data'=>$data,
									'states'=>$states,
									'sortFields'=>$this->sortFields,
									'sortDir'=>$this->sortDir 
									)
						);  
		}
		else
		{
			return \View::make($this->pageView);
		}
	}

	/**
	 * 
	 * Default Add function
	*/
	function getAdd()
	{

		if($this->getCheckPermission($this->module,$this->can_add))
		{
			return \View::make($this->addView);
		}
		else
		{
			return \View::make($this->pageView);
		}
	}

	/**
	 * Get Edit Default function
	*/
	function getEdit($id=0)
	{
		if($this->getCheckPermission($this->module,$this->can_edit))
		{
			$message = array(
							'success'=>true,
							'message'=>''
							);
			if($id)
			{
				$model = $this->model();

				$data['item'] = $model->getItem($id);
				
				if(!count($data['item'])) 
				{
					throw new NotFoundException("The following item key ($id) could not be found !");
				}
				return View::make($this->editView,$data);
			}
			else
			{
				
				$message['message'] = Lang::get($this->tageName.".unable_to_edit");
				$message['success'] = false;
				\Session::flash('message', 
					$message
					);
				return \Redirect::action($this->defaultBackUrl);
			}
		}
		else
		{
			return \View::make($this->pageView);
		}
	}
	function getBackUrl()
	{
		return $this->backUrl;
	}
	function addLog($data,$action)
	{
		$user = Auth::users()->get();
		$user_id = isset($user) ? $user->id : null;
		$userLog = new \UserLog();
		$userLog->addLog($user_id,$data,$this->tageName,$action);

	}

	function postSave($id=0)
	{
		$this->key = $id;
		$message = array(
					'success'=>true,
					'message'=>trans($this->tageName.'.item_saved')
					);

	/*	try{*/

			$data 	= Input::get($this->tageName,array());
			$model  = $this->model();//var_dump($data);exit();
			$this->key = $model->saveAll($id,$data);
			$action = 'edit';
			if(!$id)
			{
				$action = 'add new';
			}
			$this->addLog($data,$action);

			\Session::flash('message', $message);
			return \Redirect::action($this->defaultBackUrl);

		/*}catch(Exception $e)f
		{

			$message = array(
					'success'=>false,
					'message'=>$e->getMessage()
					);
			\Session::flash('message', $message);

			return \Redirect::action($this->getBackUrl($id),$id);
		}*/
		

	}

	function beforeDelete($id)
	{

	}
	function getDelete($id)
	{
		if($this->getCheckPermission($this->module,$this->can_delete))
		{
			$message = array(
						'success'=>true,
						'message'=>trans($this->tageName.'.delete_success')
						);
				
			try  

			{

				$this->addLog(array('id'=>$id),'delete');
				$model 	= $this->model();
				$model->id  = $id;
				$this->beforeDelete($id);
				$model->exists = true;
				$model->delete();

				
			} catch (Exception $e) 
			{
				$message['success'] = false;
				$message['message'] = $e->getMessage();
			}
			\Session::flash('message', $message);		

			return \Redirect::action($this->defaultBackUrl);
		}
		else
		{
			return \View::make($this->pageView);
		}
			
	}
	public function postDeleteList()
	{
		if($this->getCheckPermission($this->module,$this->can_delete))
		{
			$lst_id = Input::get('ckb', array());
			$message = array(
						'success'=>true,
						'message'=>trans($this->tageName.'.delete_success')
						);
				
			try {
				$this->addLog(array('id'=>$lst_id),'delete list');
				for($i=0;$i<count($lst_id);$i++)
				{
					$this->beforeDelete($lst_id[$i]);
					$model 	= $this->model();
					$model->id  = $lst_id[$i];
					$model->exists = true;
					$model->delete();

				}
			} catch (Exception $e) 
			{
				$message['success'] = false;
				$message['message'] = $e->getMessage();
			}
			\Session::flash('message', $message);		

			return \Redirect::action($this->defaultBackUrl);
		}
		else
		{
			return \View::make($this->pageView);
		}
	}
	public function getState($id,$state)
	{
		if($this->getCheckPermission($this->module,$this->can_edit))
		{
			$message = array(
						'success'=>true,
						'message'=>''
						);

			try 
			{


				$model 	= $this->model();
				$state = !$state;
				$this->addLog(array('state'=>$state),'update state');
				$model->state($id,$state);
				$msg  = $state?trans($this->tageName.'.activate_success'):trans($this->tageName.'.deactivate_success');
				$message['message'] = $msg;


			} catch (Exception $e) 
			{
				$message['message'] = trans( $e->getMessage() );	
			}
			\Session::flash('message', $message);		
			return \Redirect::action($this->defaultBackUrl);
		}
		else
		{
			return \View::make($this->pageView);
		}
	}
	public function getView($id)
	{	
		if($this->getCheckPermission($this->module,$this->can_view))
		{
			$model 	= $this->model();
			$item 	= $model->getItem($id);
			if(is_null($item) || !count($item)) throw new NotFoundException("Item could not be found !", null);
			
			return \View::make($this->viewView,array('item'=>$item));
		}
		else
		{
			return \View::make($this->pageView);
		}
	}

	public function postActiveList($type=0)
	{
		if($this->getCheckPermission($this->module,$this->can_edit))
		{
			$message = array(
						'success'=>true,
						'message'=>''
						);
			$type = !$type;
			try 
			{
				$lst_id = Input::get('ckb',array());
				$this->addLog(array('id'=>$lst_id,'state'=>$type),'update state list');
				$model 	= $this->model();
				if(!count($lst_id))
				{
					throw new Exception($this->tageName.".check_lst_box_first", 1);		

				}
				$msg  = $type?trans($this->tageName.".activate_list_success"):trans($this->tageName.".deactivate_list_success");
				$model->updateStates($lst_id,$type);
				$message['message']	 = $msg;

				
			}catch (Exception $e) 
			{
					$message['message'] = trans($e->getMessage());
					$message['success'] = false;
					
			}
			\Session::flash('message', $message);		
			return Redirect::action($this->defaultBackUrl);
		}
		else
		{
			return \View::make($this->pageView);
		}

	}
	
	// add allow png for file param
	public function fileParams($folder,$params, $allow_png = true)
	{
		$new_params = array();
		$defaultWidth = 500;
		$defaultHeight = 500;
		$folder_path  = public_path()."/$folder";
		if(!is_dir($folder_path) && !file_exists($folder_path))
		{
			mkdir($folder_path);
		}
		foreach ($params as $key => $value) 
		{
			
			$tmp_file = \Input::get("tmp_{$key}", '');				

			if($tmp_file=='delete')
			{
				$new_params[$key] = '';
				$path = "{$folder}/{$value}";
				$full_path = public_path()."/$path";	
				if(file_exists($full_path) && !is_dir($full_path))
				{
					unlink($full_path);
					
				}
				if(isset($this->resize[$key]))
					{
						
						$this->doRemoveImages(
										$full_path,
										public_path()."/$folder",
										$this->resize[$key]
										);
					}
				
			}
			else
			{
				$path 		= "tmp/{$tmp_file}";
				$copy_name 	= $value;				
				$new_params[$key] = $copy_name;				
				if(!$copy_name) $copy_name = $tmp_file;
				$file_path = public_path()."/tmp/{$tmp_file}";				
				if(file_exists($file_path) && !is_dir($file_path))
				{
					$new_path  = public_path()."/{$folder}/{$copy_name}";
					copy(public_path()."/tmp/{$tmp_file}", $new_path);
					unlink(public_path()."/tmp/{$tmp_file}");
					$new_params[$key] = $copy_name;
					if(isset($this->resize[$key]))
					{
						$this->doresizeImage(
												$new_path,
												public_path()."/{$folder}",
												$this->resize[$key],
												true,
												$allow_png
											);
					}
					
				 /*md5(date('Y-m-d H:i:s').rand(1,100)); */
				}
			}

		}
		
		
		return $new_params;
	}
	/**
	*
	* Function resize image 
	* use to resize some images that we
	* need to make thumbnail base on options 
	* @author Engleang
	* @param String $imgName
	* @param Sring $folder
	* @param Array $options
	*
	*/
	protected function doresizeImage($imgName,$folder,$options,$not_keep = true, $image_png = true, $quality = 60)
	{
		$width 	= null;
		$height = null;
		// $quality = 80;
		$defaultPath = public_path().'/images/default_background.png';
		
		//load background image
		// $log = new Logger('Vehicle');
		// $log->pushHandler(new StreamHandler(app_path().'/storage/logs/dealer.log', Logger::WARNING));

		// 	// add records to the log
		// $log->addWarning('Data',$data);

		$options['quality'] = $quality;
		
		if(isset($options['quality']))
		{
			$quality = (int)$options['quality'];
			
		}
		if(isset($options['width']))
		{
			$width = $options['width'];
			unset($options['width']);

		}
		if(isset($options['height']))
		{
			$height = $options['height'];
			unset($options['height']);
			
		}
		
		if($width ==null &&$height == null)
		{
			$width  = 500;
			$height = 500;

		}

		$tmpPath  = public_path().'/images/bg_'.$width.'_'.$height.'.png';
		if(file_exists($tmpPath) && !is_dir($tmpPath))
		{
			$defaultPath = $tmpPath;
		}

		$defaultBackground = \Image::make($defaultPath);
		$asp = $width/$height;
 		$defaultBackground->resize($width,$height);	
		$path 	= $imgName;
		// check if png is allowed
		if ($image_png)
		{
			$newImgName = $imgName;
		}
		// inf not png, generate jpg
		else
		{
			$newImgName = substr($imgName, 0, strlen($imgName) - 4) . '.jpg';
		}
 		$baseImg = \Image::make($path);	
 		$width = $baseImg->width();
 		$height = $baseImg->height();
 		$newAsp  = $width/$height; 		
 		
 		$new_path = $path; 		
 		
 		foreach ($options as $key => $value) 
 		{
 			if(!isset($value['width']) && !isset($value['height']))continue;
 		// 	$quality = 100;
			// if(isset($value['quality']))
			// {
			// 	$quality = (int)$value['quality'];
			// }
			$imgBackground = \Image::make($defaultPath);//load background image
			$img = \Image::make($path);	 			
 			$width 		= $value['width'];
 			$height 	= $value['height'];
 			$imgBackground->resize($width,$height);
 			$asp = $width/$height;
 			$heightToResize = $height;
 			$widthToResize = $width;
 			if($newAsp>$asp) $heightToResize = null;
 			else $widthToResize = null;
			$new_folder = "{$folder}/$key";
			if(!file_exists($new_folder) && !is_dir($new_folder))
			{
				mkdir($new_folder);
			}
			$path_info = pathinfo($newImgName);			
	 		$img->resize($widthToResize,$heightToResize,function ($constraint) 
	 		{
 			   $constraint->aspectRatio();
			});
	 		$new_path = $new_folder."/".$path_info['basename'];
	 		//var_dump($new_path);
	 		//exit();
	 		// $img->save($new_path,$quality);
	 		$imgBackground->insert($img,'center')->save($new_path,$quality);	

 		}	
 		if($not_keep)
 		{

 			unlink($imgName);	
 			
 		}
 		else
 		{
 			$baseImg->resize($width,$height);	
	 		$widthToResize = $width;
	 		$heightToResize = $height;
	 		if($newAsp>$asp)
	 		{
	 			$heightToResize = null;
	 		}
	 		else
	 		{
	 			$widthToResize = null;
	 		}
	 		$baseImg->resize($widthToResize,$heightToResize,function ($constraint) {
    			$constraint->aspectRatio();
			});
 			$defaultBackground->insert($baseImg,'center')->save($new_path,$quality);	
 		}
 		



	}
	/**
	*
	* Function doRemoveImage use to remove images that we create thumbnail folder
	* @author Engleang
	* @param String $imgName
	* @param Sring $folder
	* @param Array $options
	*
	*/
	protected function doRemoveImages($imgName,$folder,$options)
	{
		$path_info = pathinfo($imgName);
		$base_name  = $path_info['basename'];
		$full_path  = "$folder/$base_name";
		if(file_exists($full_path) && !is_dir($full_path))
		{
			unlink($full_path);
		}
		
		if(isset($options['width']))unset($options['width']);
		if(isset($options['height']))unset($options['height']);
		foreach ($options as $key => $value) 
		{
			$full_path = "$folder/$key/$base_name";	
			if(file_exists($full_path) && !is_dir($full_path))
			{
				unlink($full_path);
			}

		}
		
	}
	



}
