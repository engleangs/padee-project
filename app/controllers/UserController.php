<?php 
/*************************************************
*	*File Name: Agent Controller
*	*Functionality: 
	*History:

		- 2015-06-01 Sim Chhayrambo Initial Version

*	*Developed & designed By : ABI-Technologies
*	
*************************************************/
class UserController extends \BaseController
{
	protected  $indexView = 'user.index';
 	protected  $editView  = 'user.edit';
 	protected  $addView   = 'user.add';
 	protected  $viewView  = 'user.view';
 	protected  $modalView = 'user.modal';
 	protected  $modelName = "Agent";
 	protected  $module 	 = 'user';
 	protected  $can_add   ='can_add';
	protected  $can_view  ='can_view';
	protected  $can_edit  = 'can_edit';
	protected  $can_delete='can_delete';
 	protected  $defaultBackUrl = "UserController@anyIndex";
 	protected  $tageName = "admin_agent";
 	protected  $pageView  = 'nopermission.index';
 	protected  $sortFields = [
	 						'first_name'=>'admin_agent.name',
	 						'type_id'=>'admin_agent.type_id',
	 						'position' =>'admin_agent.position',
	 						'state'=>'admin_agent.state',
	 					];
	protected $resize = [
 			'featured_image'=>[
 					'100_100'=>[
 						'width'=>100,
 						'height'=>100,
 					],
 					'200_200'=>[
 						'width'=>200,
 						'height'=>200,
 					],
 					
 			],
 	];


 	public function getCheckPermission($module,$action)
 	{
 		$permission = UserHelper::checkPagePermission($module,$action);
 		$check = false;
 		if($permission)
 			$check = true;
 		return $check;
 	}
 	public function getListPermission()
 	{
 		$user_permission = Session::get('agent_permission',array());
 		$admin_level = Session::get('admin_level');
 		$display = false;
 		if ((isset($user_permission[$this->module]['can_view']) && $user_permission[$this->module]['can_view']) || $admin_level)
 		{
 			$display = true;
 		}
 		elseif ((isset($user_permission[$this->module]['can_edit']) && $user_permission[$this->module]['can_edit']) || $admin_level) {
 			$display = true;
 		}
 		return $display;
 	}
 	public function beforeDelete($id)
	{
		$model = $this->model();
		$obj   =	$model->find($id);
		if(!$obj) throw new Exception(trans('admin_default.item_not_found'), 404);

		$this->doRemoveImages($obj->featured_image,public_path().'/resources/agent', $this->resize['featured_image']);
	
	}
 	public function getLogin()
	{
		return \View::make('user.login');
	}
 	function getBackUrl($id=0)
 	{
 		$editprofile = Input::get('sender',null);
 		if(!$id)
 		{
 			return "UserController@getAdd";
 		}	
 		elseif($id && $editprofile == 'editprofile')
 		{
 			return "UserController@getEditProfile";
 		}
 		else{
 			return "UserController@getEdit";
 		}
 	}
 	public function getAdd()
 	{
	 	
 		if($this->getCheckPermission($this->module,$this->can_add))
 		{
	 		$categories = AgentType::getAgentTypeName();
	 		$obj 		=	new AgentPermission();
			$per_data	=	$obj->getPermission();
	 		return \View::make($this->addView)
	 		                  ->with('categories',$categories)
	 		                  ->with('item',$per_data);
	 	}
	 	else
	 	{
	 		return \View::make($this->pageView);
	 	}
 	}
 	public function getView($id=0)
 	{
 		if($this->getCheckPermission($this->module,$this->can_view))
 		{
	 		$user = Auth::users()->get();
	 		$view = parent::getView($id);
	 		$data = $view->getData('item');
	 		return $view;
	 	}
	 	else
	 	{
	 		return \View::make($this->pageView);
	 	}
 	}
 	public function getEdit($id=0)
 	{
 		if($this->getCheckPermission($this->module,$this->can_edit))
 		{
			$per_data	=	$obj->getItem($id);
	 		return parent::getEdit($id);
		}
		else
		{
			return \View::make($this->pageView);
		}
	}
 	public function anyIndex()
 	{
 		if($this->getListPermission())
 		{
		 	$status = Input::get('state',null);
			if(!is_null($status))
			{ 
				$this->setState('search.state',$status);
			}
			$stateStatus=$this->state('search.state',null);
			if($stateStatus!=='')
			{
				$this->condition['status'] = $stateStatus;
			}
	 		$view = parent::anyIndex();
	 		return $view;
		}
		else
		{
			return \View::make($this->pageView);
		}	
 	}
 	public  function postSave($id=0)
 	{
 		try 
	  	{
	  		$this->key = $id;
			$data 	= Input::get($this->tageName, array());

			$model = $this->model();
			$this->key = $model->saveAll($id,$data);

     		$message['message'] = trans('admin_agent.save_success');
			$message['success'] = true;
			
		} catch (\Exception $e) 
		{
				$message['message'] = $e->getMessage();
				$actionUrl =  $this->getBackUrl($id);
		}

		\Session::flash('message',$message);
		return \Redirect::action($actionUrl,array('id'=>$id));

 	}
 	public function postVerify()
	{
		$message = array(
				'success'=>true,
				'message'=>''
				);
		$userdata = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );
	    if(Auth::users()->attempt($userdata,false)) 
        {
        	$objuser = Auth::users()->get();

        	if($objuser->state==1 && $objuser->is_deleted == 0)
        	{
	        	$obj = new User();
	        	if($objuser->is_super_admin == 1)
	        	{
	        		Session::set('is_super_admin',$objuser->is_super_admin);
	        	}
	        	else
	        	{
	        		$data = $obj->getPermission($objuser->id);
	        		Session::set('user_permission',$data);
	        	}
	            return Redirect::to('/');
        	}
        	else
        	{
        		$message['message'] = trans('admin_user.disable_user');
				$message['success'] = false;
				Session::flash('message', 
				$message
				); 
            	return Redirect::action("UserController@getLogin");
        	}
        	
        }
        else
        {
            $message['message'] = trans('admin_user.invalid_user');
			$message['success'] = false;
			Session::flash('message', 
				$message
				); 
            return Redirect::action("UserController@getLogin");
        }
	}
	public function getLogout()
	{
		\Auth::users()->logout();
		Session::clear();
		return \Redirect::action('UserController@getLogin');
	}
	public function getEditProfile()
	{
		$data['user']  = Auth::users()->get();
 		return View::make('user.editprofile',$data);
	}
	public function getProfile()
	{
		$message = array(
				'success'=>true,
				'message'=>''
				);
		$data['user'] = Auth::users()->get();
		return \View::make('user.profile',$data);
	}
	public function getCheckExistEmail()
 	{
 		return 1;
 	}
 	public function getCheckExistUserName()
 	{
 		return 1;
 	}
 	public function getDelete($id)
 	{
 		if($this->getCheckPermission($this->module,$this->can_delete))
		{
			$message = array(
						'success'=>true,
						'message'=>trans($this->tageName.'.delete_success')
						);
			try 
			{
				$model = $this->model();
 				$obj   =	$model->find($id);
 				if(!$obj) throw new \Exception(trans('admin_default.item_not_found'), 404);
 				\DB::table('users')
 					 ->where('id','=',$id)
 					 ->update(['is_deleted'=> 1]);
			} catch (Exception $e) 
			{
				$message['success'] = false;
				$message['message'] = $e->getMessage();
			}
			\Session::flash('message', $message);		

			return \Redirect::action($this->defaultBackUrl);
		}
		else
		{
			return \View::make($this->pageView);
		}
 	}
 	public function postDeleteList()
	{
		return \View::make($this->pageView);
	}
 	 	
}

?>
