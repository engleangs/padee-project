<?php

/*
|--------------------------------------------------------------------------
| Register The Laravel Class Loader
|--------------------------------------------------------------------------
|
| In addition to using Composer, you may use the Laravel class loader to
| load your controllers and models. This is useful for keeping all of
| your classes in the "global" namespace without Composer updating.
|
*/
date_default_timezone_set("Asia/Bangkok");
ClassLoader::addDirectories(array(

	app_path().'/commands',
	app_path().'/controllers',
	app_path().'/models',
	app_path().'/database/seeds',
	app_path().'/helpers',
	app_path().'/exceptions',


));

/*
|--------------------------------------------------------------------------
| Application Error Logger
|--------------------------------------------------------------------------
|
| Here we will configure the error logger setup for the application which
| is built on top of the wonderful Monolog library. By default we will
| build a basic log file setup which creates a single file for logs.
|
*/

Log::useFiles(storage_path().'/logs/laravel.log');

/*
|--------------------------------------------------------------------------
| Application Error Handler
|--------------------------------------------------------------------------
|
| Here you may handle any errors that occur in your application, including
| logging them or displaying custom views for specific errors. You may
| even register several error handlers to handle different types of
| exceptions. If nothing is returned, the default error view is
| shown, which includes a detailed stack trace during debug.
|
*/

App::error(function(Exception $exception, $code)
{
	Log::error($exception);
});
/*App::error(function (NotFoundException $exception,$code){
	return Response::make(
		View::make('errors.404'),404);
});*/
App::missing(function($exception)
{
    return Response::view('errors.404', array(), 404);
});
/*
|--------------------------------------------------------------------------
| Maintenance Mode Handler
|--------------------------------------------------------------------------
|
| The "down" Artisan command gives you the ability to put an application
| into maintenance mode. Here, you will define what is displayed back
| to the user if maintenance mode is in effect for the application.
|
*/

App::down(function()
{
	return Response::make("Be right back!", 503);
});

/*
|--------------------------------------------------------------------------
| Require The Filters File
|--------------------------------------------------------------------------
|
| Next we will load the filters file for the application. This gives us
| a nice separate location to store our route and application filter
| definitions instead of putting them all in the main routes file.
|
*/

require app_path().'/filters.php';

Event::listen('vehicle.add',
		function($vehicle)
		{
			$dealer_id  =  $vehicle->dealer_id;
			\Dealer::whereId($dealer_id)->update(['total_vehicle'=>DB::raw('total_vehicle+1')]);
			$trim_id 	= $vehicle->trim_id;
			\Trim::whereId($trim_id)->update(['total_vehicle'=>DB::raw('total_vehicle+1')]);
			$model_id 	= $vehicle->model_id;
			\Model::whereId($model_id)->update(['total_vehicle'=>DB::raw('total_vehicle+1')]);
			$model = \Model::whereId($model_id)->first();
			if($model)
			{
				if(!$vehicle->is_expired)
				{
					\Make::whereId($model->make_id)->update(['total_vehicle'=>DB::raw('total_vehicle+1')]);
				}
			}




			
		}
	);
Event::listen('vehicle.beforeUpdate',
	function($id,$newVehicleData)
	{
		$objVehicle = \Vehicle::find($id);
		$model = new Model();
		if($objVehicle->model_id == $newVehicleData->model_id)
		{
			//the same model
			if($objVehicle->is_expired ==0 && $newVehicleData->is_expired ==1)
			{
				$model->decreaseTotalVehicle($objVehicle->model_id);
			}
		}
		else
		{
			//different model
			if($objVehicle->is_expired==0)
			{
				//decrease old model
				$model->decreaseTotalVehicle($objVehicle->model_id);
			}
			if($newVehicleData->is_expired==0)
			{
				//increase new model
				$model->increaseTotalVehicle($newVehicleData->model_id);
			}

		}
		


	}
);
Event::listen('vehicle.delete',function($vehicle)
{
			$dealer_id  =  $vehicle->dealer_id;
			\Dealer::whereId($dealer_id)
					->where('total_vehicle','>',0)
					->update(['total_vehicle'=>DB::raw('total_vehicle-1')]);
			$trim_id 	= $vehicle->trim_id;
			\Trim::whereId($trim_id)
					->where('total_vehicle','>',0)
					->update(['total_vehicle'=>DB::raw('total_vehicle-1')]);
			$model_id 	= $vehicle->model_id;
			if(!$vehicle->is_expired)
			{
				\Model::whereId($model_id)
					->where('total_vehicle','>',0)
					->update(['total_vehicle'=>DB::raw('total_vehicle-1')]);	
			}			
});
Event::listen('event.view', function($event)
{
		\Deal::whereId($event->id)->update(['hits'=>DB::raw('hits+1')]);
});
Event::listen('business.view', function($business)
{
		\Business::whereId($business->id)
				->update(['hits'=>DB::raw('hits+1')]);		
});
Event::listen('healthissue.view', function($healthissue)
{
		\HealthIssue::whereId($healthissue->id)
				->update(['hits'=>DB::raw('hits+1')]);		
});
Event::listen('property.view', function($property)
{
		\Property::whereId($property->id)->update(['hits'=>DB::raw('hits+1')]);
});
Event::listen('vehicle.view', function($vehicle)
{
		\Vehicle::whereId($vehicle->id)->update(['hits'=>DB::raw('hits+1')]);
});
Event::listen('post.view', function($post)
{
		\Post::whereId($post->id)->update(['hits'=>DB::raw('hits+1')]);
});
Event::listen('jobseeker.view',function($jobseeker){
	\JobSeeker::whereId($jobseeker->id)->update(['hits'=> DB::raw('hits+1')]);
});
Event::listen('joboffer.view',function($joboffer){
	\JobOffer::whereId($joboffer->id)->update(['hits'=> DB::raw('hits+1')]);
});
Event::listen('review.view', function($vehicle)
{
		\Vehicle::whereId($vehicle->id)->update(['review_hits'=>DB::raw('review_hits+1')]);
});
Event::listen('service.view', function($service)
{
		$service_id = $service->id;
		\Service::whereId($service_id)
				->update(['hits'=>DB::raw('hits+1')]);		
		$serviceView = new \ServiceView(['service_id'=>$service_id]);
		$serviceView->save();
		

});



 ?>
