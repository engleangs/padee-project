<?php
 
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

if(in_array(Request::segment(1),Config::get('app.alt_lang')))
{
	App::setLocale(Request::segment(1, 'en'));
	Config::set('app.local_prefix',Request::segment(1, 'en'));
}
else
{
	$cookie  = Cookie::get('local_prefix', null);
	if(Cookie::has('local_prefix'))
	{
		Config::set('app.local_prefix',Cookie::get('app.local_prefix', 'en'));
		
	}
	Route::get('/',function(){
			return Redirect::to('/'.Config::get('app.local_prefix','en'));
		});


}
Route::get('login','UserController@getLogin');
Route::post('login','UserController@postVerify');
Route::get('logout', 'UserController@getLogout');
Route::group
		(array(
				'prefix'=>Config::get('app.local_prefix', ''),
				'before'=>'lang_handler',
	)
	, 
	function(){

		App::setLocale(Config::get('app.local_prefix', 'en'));
		Route::get('/', 'HomeController@anyIndex');
		Route::controller('user', 'UserController');
		Route::controller('indicator', 'IndicatorController');
		// Route::controller('dashboard','DashboardController'); 
});