<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> PADEE : @yield('title')</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        <link href="{{asset('packages/abi/administrator/css/bootstrap.css')}}" 
                rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="{{asset('packages/abi/administrator/css/font-awesome.min.css')}}" 
                rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="{{asset('packages/abi/administrator/css/ionicons.min.css')}}" 
                rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="{{asset('packages/abi/administrator/css/morris/morris.css')}}" 
                rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="{{asset('packages/abi/administrator/css/jvectormap/jquery-jvectormap-1.2.2.css')}}" 
                rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="{{asset('packages/abi/administrator/css/fullcalendar/fullcalendar.css')}}" 
                rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="{{asset('packages/abi/administrator/css/daterangepicker/daterangepicker-bs3.css')}}" 
                rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="{{asset('packages/abi/administrator/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}" 
                rel="stylesheet" type="text/css" />
        <link href="{{asset('packages/abi/administrator/css/datatables/dataTables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('packages/abi/administrator/css/chosen/chosen.min.css')}}" rel="stylesheet" type="text/css" />
        
        <!-- Theme style -->
        <link href="{{asset('packages/abi/administrator/css/AdminLTE.css')}}" 
                rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="{{asset('packages/abi/administrator/js/plugins/notification/alertify.core.css')}}" >
        <link rel="stylesheet" type="text/css" href="{{asset('packages/abi/administrator/js/plugins/notification/alertify.default.css')}}">
        <link href="{{asset('packages/abi/administrator/css/abi.css')}}" 
                rel="stylesheet" type="text/css" />
         <link rel="stylesheet" type="text/css" href="{{asset('packages/abi/administrator/css/datepicker.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('packages/abi/administrator/css/datepicker3.css')}}">
        
        <style type="text/css">
            .panel-foot {
                background-color: #f5f5f5;
                border-bottom-left-radius: 3px;
                border-bottom-right-radius: 3px;
                border-top: 1px solid #dddddd;
                padding: 10px 15px 10px 190px;
    }
    .note-message,.control-label span{
        color: #ff0000;
        font-style: italic;
    }
    .more-action
    {
        cursor: pointer;
        color: #0000ff;
    }
    .view tr,.view tr td
    {
        padding: 5px;
    }
    .nav-tabs-custom > .nav-tabs > li.active > a {
    border-left-color: #000;
    border-right-color: #000;
    border-top: 0 none;
    }
    .nav-tabs-custom
    {
        box-shadow: 0 4px 14px rgba(0, 0, 0, 0.1);
    }
    .cwrapper
    {
        background-color: #f4f4f4;
    }
    .box-title
    {
        border-bottom: 1px #f4f4f4 solid;
        width: 100%;
    }
    .thumb
    {
        width: 200px;
        margin-bottom: 5px;
    }
    .img-wrapper
    {
        padding: 3px;
    }
    .align-left
    {
        text-align: left !important;
    }
    .btn-browse img
    {
        display: none;
    }
    .paginate-wrapper ul
    {
        margin: 0px 10px !important;  
    }
    .date-desc
    {
        font-style: italic;
        font-size: 12px;
        color: #bbb;
        cursor: pointer;
    }
    th span
    {
        width: 100%;
    }
    .tbl-view
    {
    	width: 100%;
    	height:100%;
    }
    .tbl-view tr
    {
        padding: 3px;
        margin: 5px;
        border-bottom : 1px solid #f4f4f4;
    }
    .tbl-view tr:last-child
    {
       
        border-bottom : none;
    }
    .tbl-view tr td,.tbl-view tr th
    {
        padding: 3px;
        margin: 2px;
        text-align:left;
        vertical-align: top;
    }
    .tbl-view p
    {
        margin: 0px !important;
    }
    .box-title
    {
        cursor: pointer;
    }
    .img-gallery
    {
        margin: 0px;
        padding: 1px;
    }
    .img-gallery .img-thumbnail
    {
        height: 74px;
        width: 100%;
        text-align: center;
        margin: 0px;
    }
    .btn-remove-gallery,.btn-add-gallery
    {
        position: absolute;
        left: 10%;
        top:30%;
    }
    
    .img-gallery .btn-remove-gallery,.img-gallery .g-loader
    {
        display: none;
    }
    .modal-body {
        position: relative;
        padding: 0px;
    }
    

        </style>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
           <script type="text/javascript">
            var locale = "{{Cookie::get('app_lang', 'en')}}";
            
        </script>
    </head>
    <body class="skin-blue">
        <!-- header logo: style can be found in header.less -->
        <div class="wrapper row-offcanvas row-offcanvas-left">
           

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside>
                
                <section class="content">
                      {{\Helper::getFlashMessage()}}

                    @yield('content')
                </section>
            </aside>

        </div><!-- /.row (main row) -->
      
    

        <!-- add new calendar event modal -->


        <!-- jQuery 2.0.2 -->
        <script src="{{asset('packages/abi/administrator/js/jquery.min.js')}}"></script>
        <!-- jQuery UI 1.10.3 -->
        <script src="{{asset('packages/abi/administrator/js/jquery-ui-1.10.3.min.js')}}" 
                type="text/javascript"></script>
        <!-- Bootstrap -->
        <script src="{{asset('packages/abi/administrator/js/bootstrap.min.js')}}" 
                type="text/javascript"></script>
        <!-- Morris.js charts -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        
        <!-- Sparkline -->
        <script src="{{asset('packages/abi/administrator/js/plugins/sparkline/jquery.sparkline.min.js')}}" 
                type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="{{asset('packages/abi/administrator/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}" 
                type="text/javascript"></script>
        <script src="{{asset('packages/abi/administrator/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}" 
                type="text/javascript"></script>
        <!-- fullCalendar -->
        <script src="{{asset('packages/abi/administrator/js/plugins/fullcalendar/fullcalendar.min.js')}}" 
                type="text/javascript"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{asset('packages/abi/administrator/js/plugins/jqueryKnob/jquery.knob.js')}}" 
                type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="{{asset('packages/abi/administrator/js/plugins/daterangepicker/daterangepicker.js')}}" 
                type="text/javascript"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{asset('packages/abi/administrator/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}" 
                type="text/javascript"></script>
        <!-- iCheck -->
        <script src="{{asset('packages/abi/administrator/js/plugins/iCheck/icheck.min.js')}}" 
                type="text/javascript"></script>
        <!-- iCheck -->
        <script src="{{asset('packages/abi/administrator/js/bootbox.js')}}" 
                type="text/javascript"></script>

        <!-- AdminLTE App -->
        <script src="{{asset('packages/abi/administrator/js/AdminLTE/app.js')}}" 
                type="text/javascript"></script>
        
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="{{asset('packages/abi/administrator/js/AdminLTE/dashboard.js')}}" 
                type="text/javascript"></script>     
        <script src="{{asset('packages/abi/administrator/js/plugins/datatables/jquery.dataTables.js')}}" type="text/javascript"></script>
        <script src="{{asset('packages/abi/administrator/js/plugins/datatables/dataTables.bootstrap.js')}}" type="text/javascript"></script>
        <script src="{{asset('packages/abi/administrator/js/chosen.jquery.min.js')}}" type="text/javascript"></script>
          <script type="text/javascript" src="{{asset('js/plugins/locale/en.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/plugins/locale/en.extra.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/plugins/locale/fr.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/plugins/locale/fr.extra.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/plugins/locale/kh.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/plugins/locale/kh.extra.js')}}"></script>
        <script src="{{asset('packages/abi/administrator/js/parsley.min.js')}}" type='text/javascript'></script>     
        
        <!-- AdminLTE for demo purposes -->
        <script src="{{asset('packages/abi/administrator/js/AdminLTE/demo.js')}}" 
                type="text/javascript"></script>
        <script src="{{asset('packages/abi/administrator/js/plugins/ckeditor/ckeditor.js')}}" type="text/javascript"></script>     
        <script src="{{asset('packages/abi/administrator/js/jquery.form.js')}}" type="text/javascript"></script>     
        <script src="{{asset('packages/abi/administrator/js/abi/admin.js')}}" type="text/javascript"></script>
        <script src="{{asset('packages/abi/administrator/js/plugins/notification/alertify.js')}}" type="text/javascript"></script>
        
        <script src="{{asset('packages/abi/administrator/js/bootstrap-datepicker.js')}}" type="text/javascript"></script>

        @yield('script') 

    </body>
</html>