<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>PADEE</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Styles -->
        <link href='assets/css/chosen.css' rel='stylesheet' tyle="text/css">
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="assets/css/theme/avocado.css" rel="stylesheet" type="text/css" id="theme-style">
        <link href="assets/css/prism.css" rel="stylesheet/less" type="text/css">
        <link href='assets/css/fullcalendar.css' rel='stylesheet' tyle="text/css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,600,300' rel='stylesheet' type='text/css'> 
        <style type="text/css">
        body { padding-top: 102px; }
        </style>
        <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- JavaScript/jQuery, Pre-DOM -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> 
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="assets/js/charts/excanvas.min.js"></script>
        <script src="assets/js/charts/jquery.flot.js"></script>
        <script src="assets/js/jquery.jpanelmenu.min.js"></script>
        <script src="assets/js/jquery.cookie.js"></script>
        <script src="assets/js/avocado-custom-predom.js"></script>
    </head>
    <body class="bg_image">
        @yield('content')
        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        @yield('script') 
    </body>
</html>