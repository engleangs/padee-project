<?php 
/*************************************************
*   *File Name: Dashboard
*   *Functionality: 
    *History:

    - 2015-08-25 Sim Chhayrambo Initial Version

*   *Developed & designed By : ABI-Technologies
*   
*************************************************/

?>

@extends('layouts.default')

@section('title',trans('admin_dashboard.dashboard'))

@section('header', trans('admin_dashboard.dashboard'))

@section('content')

<?php 
// var_dump($data); 
// var_dump($item->indicator_id);
?>

<!-- Forms -->
<div class="row-fluid">

	<!-- Forms: Box -->
	<div class="span12">

		<!-- Forms: Top Bar -->
		<div class="top-bar">
			<h3><i class="icon-zoom-in"></i> M&E Indicator Detail</h3>
		</div>
		<!-- / Forms: Top Bar -->

		<!-- Forms: Content -->
		<div class="well no-padding">

			<!-- Forms: Form -->
			<form class="form-horizontal">

				<!-- Forms: Indicator ID -->
				<div class="control-group">
					<label class="span2">Indicator ID</label>
					<label class="span8">{{@$item->indicator_id}}</label>
				</div>
				<!-- / Forms: Indicator ID -->

				<!-- Forms: Title -->
				<div class="control-group">
					<label class="span2">Title</label>
					<label class="span8">{{@$item->title}}</label>
				</div>
				<!-- / Forms: Title -->

				<!-- Forms: Group -->
				<div class="control-group">
					<label class="span2">Group</label>
					<label class="span8">{{@$item->group}}</label>
				</div>
				<!-- / Forms: Group -->

				<!-- Forms: Type -->
				<div class="control-group">
					<label class="span2">Type</label>
					<label class="span8">{{@$item->type}}</label>
				</div>
				<!-- / Forms: Type -->

				<!-- Forms: Component -->
				<div class="control-group">
					<label class="span2">Component</label>
					<label class="span8">{{@$item->component}}</label>
				</div>
				<!-- / Forms: Component -->

				<!-- Forms: Baseline Value -->
				<div class="control-group">
					<label class="span2">Baseline Value</label>
					<label class="span8">{{@$item->baseline_value}}</label>
				</div>
				<!-- / Forms: Baseline Value -->

				<!-- Forms: Baseline Year -->
				<div class="control-group">
					<label class="span2">Baseline Year</label>
					<label class="span8">{{@$item->baseline_year}}</label>
				</div>
				<!-- / Forms: Baseline Year -->

				<!-- Forms: Target Value -->
				<div class="control-group">
					<label class="span2">Target Value</label>
					<label class="span8">{{@$item->target_value}}</label>
				</div>
				<!-- / Forms: Target Value -->

				<!-- Forms: Target Year -->
				<div class="control-group">
					<label class="span2">Target Year</label>
					<label class="span8">{{@$item->target_year}}</label>
				</div>
				<!-- / Forms: Target Year -->

				<!-- Forms: Description -->
				<div class="control-group">
					<label class="span2">Description</label>
					<label class="span8">{{@$item->description}}</label>
				</div>
				<!-- / Forms: Description -->

				<!-- Forms: Form Actions -->
				<div class="form-actions">
					<button type="submit" class="btn btn-primary"> Save</button>
					<button type="button" class="btn"> Cancel</button>
				</div>
				<!-- / Forms: Form Actions -->

			</form> 
			<!-- / Forms: Form -->           

		</div>
		<!-- / Forms: Content -->

	</div>
	<!-- / Forms: Box -->

</div>
<!-- / Forms -->

@stop