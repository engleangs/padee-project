<?php 
/*************************************************
*   *File Name: Dashboard
*   *Functionality: 
    *History:

    - 2015-08-25 Sim Chhayrambo Initial Version

*   *Developed & designed By : ABI-Technologies
*   
*************************************************/

?>

@extends('layouts.default')

@section('title',trans('admin_dashboard.dashboard'))

@section('header', trans('admin_dashboard.dashboard'))

@section('content')

<!-- Forms -->
<div class="row-fluid">

	<!-- Forms: Box -->
	<div class="span12 form-control">

	{{Form::open(
	        array(
	            'class'=>'form-horizontal',
	            'action'=>array("IndicatorController@postSave",0),
	            'method'=>'post',
	            'id'=>'adminForm'
	    )
	)}}

		<!-- Forms: Top Bar -->
		<div class="top-bar">
			<h3><i class="icon-cog"></i> Add M&E Indicator</h3>
		</div>
		<!-- / Forms: Top Bar -->

		<!-- Forms: Content -->
		<div class="well no-padding">

			<!-- Forms: Form -->
			<form class="form-horizontal">

				<!-- Forms: Indicator ID -->
				<div class="control-group">
					<label class="control-label" for="indicator_id">Indicator ID</label>
					<div class="controls">
						<input type="text" id="indicator_id" placeholder="..." class="span6 m-wrap">
						<span class="help-inline">Some hint here</span>
					</div>
				</div>
				<!-- / Forms: Indicator ID -->

				<!-- Forms: Title -->
				<div class="control-group">
					<label class="control-label" for="title">Title</label>
					<div class="controls">
						<input type="text" id="title" placeholder="..." class="span6 m-wrap">
					</div>
				</div>
				<!-- / Forms: Title -->

				<!-- Forms: Group -->
				<div class="control-group">
					<label class="control-label" for="group">Group</label>
					<div class="controls">
						<select class="span6">
							<option value="1">Group 1</option>
							<option value="2">Group 2</option>
							<option value="3">Group 3</option>
							<option value="4">Group 4</option>
						</select>
					</div>
				</div>
				<!-- / Forms: Group -->

				<!-- Forms: Type -->
				<div class="control-group">
					<label class="control-label" for="type">Type</label>
					<div class="controls">
						<select class="span6">
							<option value="1">Type 1</option>
							<option value="2">Type 2</option>
							<option value="3">Type 3</option>
							<option value="4">Type 4</option>
						</select>
					</div>
				</div>
				<!-- / Forms: Type -->

				<!-- Forms: Component -->
				<div class="control-group">
					<label class="control-label" for="component">Component</label>
					<div class="controls">
						<select class="span6">
							<option value="1">Component 1</option>
							<option value="2">Component 2</option>
							<option value="3">Component 3</option>
							<option value="4">Component 4</option>
						</select>
					</div>
				</div>
				<!-- / Forms: Component -->

				<!-- Forms: Baseline Value -->
				<div class="control-group">
					<label class="control-label" for="baseline_value">Baseline Value</label>
					<div class="controls">
						<input type="text" id="baseline_value" placeholder="..." class="span6 m-wrap">
					</div>
				</div>
				<!-- / Forms: Baseline Value -->

				<!-- Forms: Baseline Year -->
				<div class="control-group">
					<label class="control-label" for="baseline_year">Baseline Year</label>
					<div class="controls">
						<select class="span6">
							<option value="1">2015</option>
							<option value="2">2016</option>
							<option value="3">2017</option>
							<option value="4">2018</option>
						</select>
					</div>
				</div>
				<!-- / Forms: Baseline Year -->

				<!-- Forms: Target Value -->
				<div class="control-group">
					<label class="control-label" for="target_value">Target Value</label>
					<div class="controls">
						<input type="text" id="target_value" placeholder="..." class="span6 m-wrap">
					</div>
				</div>
				<!-- / Forms: Target Value -->

				<!-- Forms: Target Year -->
				<div class="control-group">
					<label class="control-label" for="component">Target Year</label>
					<div class="controls">
						<select class="span6">
							<option value="1">2015</option>
							<option value="2">2016</option>
							<option value="3">2017</option>
							<option value="4">2018</option>
						</select>
					</div>
				</div>
				<!-- / Forms: Target Year -->

				<!-- Forms: Description -->
				<div class="control-group">
					<label class="control-label" for="description">Description</label>
					<div class="controls">
					<textarea rows="3" class="span12"></textarea>
					</div>
				</div>
				<!-- / Forms: Description -->

				<!-- Forms: Form Actions -->
				<div class="form-actions">
					<button type="submit" class="btn btn-primary"> Save</button>
					<button type="button" class="btn"> Cancel</button>
				</div>
				<!-- / Forms: Form Actions -->

			</form> 
			<!-- / Forms: Form -->           

		</div>
		<!-- / Forms: Content -->

	{{ Form::close() }}

	</div>
	<!-- / Forms: Box -->

</div>
<!-- / Forms -->

@stop