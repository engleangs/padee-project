<?php 
/*************************************************
*   *File Name: Dashboard
*   *Functionality: 
    *History:

    - 2015-08-25 Sim Chhayrambo Initial Version

*   *Developed & designed By : ABI-Technologies
*   
*************************************************/

?>

@extends('layouts.default')
@section('title',trans('admin_dashboard.dashboard'))
@section('header', trans('admin_dashboard.dashboard'))
@section('content')

<?php 
// var_dump($data); 
// exit(); 
?>

<!-- Create M&E and Messages -->
<div class="row-fluid">

	<!-- Create M&E: Box -->
	<div class="span12">

		<!-- Create M&E: Top Bar -->
		<div class="top-bar">
			<h3><i class="icon-cog"></i> M&E Indicator Listing</h3>
		</div>
		<!-- / Create M&E: Top Bar -->

		<!-- Create M&E: Content -->
		<div class="well no-padding">

			<!-- Create M&E: Add M&E Indicator -->
			<table class="data-table">
				<thead>
					<tr>
						<th>Indicator ID</th>
						<th>Title</th>
						<th>Group</th>
						<th>Type</th>
						<th>Component</th>
						<th>Baseline Value</th>
						<th>Baseline Year</th>
						<th>Target Value</th>
						<th>Target Year</th>
						<th class="right">Actions</th>
					</tr>
				</thead>
				<tbody>
					@foreach($data as $key => $value)
					<tr>
						<td>{{$value['indicator_id']}}</td>
						<td>{{$value['title']}}</td>
						<td>{{$value['group']}}</td>
						<td>{{$value['type']}}</td>
						<td>{{$value['component']}}</td>
						<td>{{$value['baseline_value']}}</td>
						<td>{{$value['baseline_year']}}</td>
						<td>{{$value['target_value']}}</td>
						<td>{{$value['target_year']}}</td>
						<td class="right">
							<a class="btn btn-danger">Delete</a>
							<a href="{{action('IndicatorController@getEdit', $value['id'])}}" class="btn btn-info">Edit</a>
							<a href="{{action('IndicatorController@getView', $value['id'])}}" class="btn btn-success">View</a>
						</td>
					</tr>
					@endforeach
				</tbody>
				<!-- <tfoot>
					<tr>
						<th>Indicator ID</th>
						<th>Title</th>
						<th>Group</th>
						<th>Type</th>
						<th>Component</th>
						<th>Baseline Value</th>
						<th>Baseline Year</th>
						<th>Target Value</th>
						<th>Target Year</th>
						<th class="right">Actions</th>
					</tr>
				</tfoot> -->
			</table>
			<!-- / Create M&E: Add M&E Indicator -->

			</div>
			<!-- / Create M&E: Content User Overview -->

		</div>
		<!-- / Create M&E: Content -->

	</div>
	<!-- / Create M&E: Box -->

</div>
<!-- / Create M&E and Messages -->

@stop