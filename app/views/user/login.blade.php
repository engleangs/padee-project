@extends('layouts.login')
@section('content')
<div class="container">  
  <form class="form-signin form-horizontal" action="{{url('login')}}" method="post" id="frmLogin">
    <div class="top-bar">
      <h3><i class="icon-leaf"></i> PADEE</h3>
    </div>
    <div class="well no-padding">
      <div class="control-group">
        <label class="control-label" for="inputName"><i class="icon-user"></i></label>
        <div class="controls">
          <input type="text" name="username" placeholder="Username">
        </div>
      </div>
      <div class="control-group">
        <label class="control-label" for="inputUsername"><i class="icon-key"></i></label>
        <div class="controls">
          <input type="password" name="password" placeholder="Password">
        </div>
      </div>
      <div class="padding">
        <button class="btn btn-primary" type="submit">Sign in</button>
        <button class="btn" type="submit">Forgot Password</button>
      </div>
    </div>
  </form>
</div> 
@stop