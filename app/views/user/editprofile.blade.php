@extends('layouts.default')
@section('title',trans('admin_agent.edit_agent'))
@section('header',trans('admin_agent.agent_management'))
@section('breadcrumb')
<ol class="breadcrumb">
        <li>
            <a href="{{action('UserController@anyIndex')}}">
                <i class="fa fa-male"></i> 
                    {{trans('admin_agent.agent')}}
            </a>
        </li>
        <li class="active"><i class="fa fa-edit"></i>{{trans('admin_agent.edit_profile')}}</li>
</ol>
@stop
@section('content')
<div class="row">
<div class="col-md-12">
	{{Form::open(
                        array(
                            'class'=>'form-horizontal',
                            'action'=>array("UserController@postSave",$user->id),
                            'method'=>'post',
                            'id'=>'adminForm'
                    )
    )}}
   
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <h3 class="panel-title">{{trans('admin_agent.edit_profile')}}</h3>
	    </div><!-- /.box-header -->
	   <div class="panel-body user_add">
			<div class="form-group">
				<label class="col-sm-2 control-label">
					{{trans('admin_default.first_name')}}: <span>*</span>
				</label>
				<div class="col-sm-9">
					<input type="text" 
						data-parsley-maxlength="30" 
						id="first_name"
						value="{{@$user->first_name}}"
						data-parsley-group="tab_1"
						data-parsley-required="true"
						placeholder="{{trans('admin_default.first_name')}}" 
						name="admin_agent[first_name]" 
						class="form-control" >
					<span class="first_name_require"></span>
				</div>
			</div>
		    <div class="form-group">
				<label class="col-sm-2 control-label">
					{{trans('admin_default.last_name')}}: <span>*</span>
				</label>
				<div class="col-sm-9">
					<input type="text" 
						data-parsley-maxlength="30"  
						id="last_name"
						value="{{@$user->last_name}}"
						data-parsley-group="tab_1"
						data-parsley-required="true"
						placeholder="{{trans('admin_default.last_name')}}" 
						name="admin_agent[last_name]" 
						class="form-control" >
					<span class="last_name_require"></span>
				</div>
			</div>
		    <div class="form-group">
				<label class="col-sm-2 control-label">
					{{trans('admin_default.position')}}: <span>*</span>
				</label>
				<div class="col-sm-9">
					<input type="text" 
						data-parsley-maxlength="50" 
						id="position" 
						value="{{@$user->position}}"
						data-parsley-group="tab_1"
						data-parsley-required="true"
						placeholder="{{trans('admin_default.position')}}" 
						name="admin_agent[position]" 
						class="form-control" >
						<span class="position_require"></span>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">
					{{trans('admin_default.email')}}: <span>*</span>
				</label>
				<div class="col-sm-9">
					<input type="email" 
						data-parsley-maxlength="100"
						data-parsley-type="email" 
						id ="email" 
						value="{{@$user->email}}"
						data-parsley-group="tab_1"
						data-parsley-required="true"
						placeholder="{{trans('admin_default.email')}}" 
						name="admin_agent[email]" 
						class="form-control" >
						<span class="email_require"></span>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">
					{{trans('admin_default.mobile_number')}}: <span>*</span>
				</label>
				<div class="col-sm-9">
					<input
						type="text"
						data-parsley-maxlength="11"
						data-parsley-type="digits" 
						maxlength ="11"
						id="phone1" 
						value="{{@$user->phone1}}"
						data-parsley-group="tab_1"
						data-parsley-required="true"
						placeholder="{{trans('admin_default.phone1')}}" 
						name="admin_agent[phone1]" 
						class="form-control" >
						<span class="phone1_require"></span>
				</div>
		    </div>
			<div class="form-group">
				<label class="col-sm-2 control-label">
					{{trans('admin_default.contact_number')}}: 
				</label>
				<div class="col-sm-9">
					<input
						type="text"
						data-parsley-maxlength="11"
						data-parsley-type="digits" 
						value="{{@$user->phone2}}"
						maxlength ="11"
						placeholder="{{trans('admin_default.phone2')}}" 
						name="admin_agent[phone2]" 
						class="form-control" >
				</div>
		    </div>
		    <div class="form-group">
					<label class="col-sm-2 control-label">{{trans('admin_user.user_name')}} <span>*</span></label>
					<div class="col-sm-9">
						<input type="text" data-parsley-maxlength="50" 
						data-parsley-required="true"
						data-parsley-group="tab_2"
						value="{{@$user->username}}"
						placeholder="{{trans('admin_user.user_name')}}" name="admin_agent[username]" id="username" class="form-control agent-login-required" >
						<span class="username_require"></span>
					</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">{{trans('admin_user.password')}}</label>
				<div class="col-sm-9">
					<input type="password"
					data-parsley-length="[6, 20]" 
          			minlength="6"
          			maxlength="20"
          			data-parsley-group="tab_2"
					placeholder="{{trans('admin_user.password')}}" name="admin_agent[password]" id="password" class="form-control agent-login-required" >
					<span class="password_require"></span>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">{{trans('admin_agent.confirm_password')}}</label>
				<div class="col-sm-9">
					<input type="password" data-parsley-equalto="#password"
					data-parsley-length="[6, 20]" 
          			minlength="6"
          			maxlength="20" 
          			data-parsley-group="tab_2"
					placeholder="{{trans('admin_agent.confirm_password')}}" name="admin_agent[confirm_password]" id="confirm_password" class="form-control agent-login-required" >
					<div class="con_message"></div>
					<span class="confirm_password_require"></span>
				</div>
			</div>
			<div class ="form-group">
				<label class="col-sm-2 control-label">
					{{trans('admin_agent.profile_image')}}
				</label>
				<div class="img-wrapper col-sm-9" id ="img-feature">
	        		<?php 
	        			$path = "resources/agent/200_200/".@$user->featured_image;
	        			$full_path 	= public_path()."/$path";
					?>
	        		<?php if(file_exists($full_path) && !is_dir($full_path)): ?>
	        			<img src="{{asset($path)}}?{{rand(1,100)}}" class="img-thumbnail thumb">
	        		<?php else: ?>
	        			<div class="img-thumbnail thumb pull-left">			
							<h2>
								{{trans('admin_default.no_image')}}
							</h2>
						</div>
	        		<?php endif; ?>
	        								
					<a href="javascript:void(0);" 
						data-form="frm-upload"
						data-hidden="tmp_image"
						class="btn btn-sm btn-primary btn-browse">
						<i class="fa  fa-folder-open-o"></i> 
						{{trans('admin_default.browse')}}
						<img src="{{asset('img/progress-small.gif')}}">
						
					</a>
					&nbsp;						
					<a 	href="javascript:void(0);" 
						data-hidden="tmp_image"
						class="btn btn-sm btn-danger btn-remove-image">
						<i class="fa fa-times"></i> 
						{{trans('admin_default.remove')}}
					</a>
					
					<div class="clearfix"></div>
		        </div>
			</div>
		</div>
		<div class="panel-foot">
				<div class="col-sm-6 pull-left padding-left-15">
					<button  class="btn btn-primary" id="btn_save"> 
						<i class="fa  fa-save"></i> {{trans('admin_default.save')}} 
					</button>
						&nbsp;
				</div>
				<div class="col-sm-4 pull-right note-message">
					<i class="pull-right">
					{{trans('admin_default.note')}}&nbsp;&nbsp;:&nbsp;&nbsp;<strong>*</strong>&nbsp;&nbsp;{{trans('admin_default.is_required')}}
					</i>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
		</div> 
	</div>
	<input type="hidden" value="editprofile" name="sender">
	<input type="hidden" name="agent_id" value="{{@$user->id}}" id="agent_id">
	<!--hidden -file -->
		<input type="hidden" name="exist_featured_image" value="{{@$user->featured_image}}">
		<!--end hidden -file-->
		<!--hidden tmp file -->
		<input type="hidden" name="tmp_featured_image" id="tmp_image">
		<!--end hidden tmp file-->
	</form>
</div>
</div>
<form 
	action="{{action('UploadController@upload')}}" 
	id="frm-upload" 
	class="frm-upload" 
	method="post" 
	enctype="multipart/form-data">
	<input 
		type="file" 
		id="file" 
		name="file_upload"
		class="file-to-upload"
		style="display:none;"
		>

</form>
<style type="text/css">
	#abi-table .help-block{
		display: none;
	}
	.email_require,.username_require{
		color: #ff0000;
	}
	.img-wrapper
	{
		margin-left: 15px;
	}
</style>
@stop
@section('script')
@parent
<script type="text/javascript"> 
  	$(document).ready(function(){
  		$('#email').on('blur',function(e){
          checkExistingEmail();
        });
  		$('#username').on('blur',function(e){
          checkExistingUserName();
        });
  		$('#btn_save').click(function(e){
  			if($('#username').hasClass('prevent_user'))
	        {
	            e.preventDefault();
	        }
            if($('#email').hasClass('prevent_email'))
            {
                e.preventDefault();
            }
  		});
  	});
    function checkExistingEmail()
    {
      $.ajax({
        url :'{{action("UserController@getCheckExistEmail")}}',
        type : 'GET',
        data :
        {
          email : $('#email').val(),
          agent_id    : $('#agent_id').val()
        },
        success:function(data)
        {
          if(data.success == true)
          {
            $('.email_require').html(data.message);
            $('#email').addClass('prevent_email');
          }
          else
          {
            $('.email_require').html('');
            $('#email').removeClass('prevent_email');
          }
        }
      });
    }
    function checkExistingUserName()
    {
      $.ajax({
        url :'{{action("UserController@getCheckExistUserName")}}',
        type : 'GET',
        data :
        {
          username : $('#username').val(),
          agent_id    : $('#agent_id').val()
        },
        success:function(data)
        {
          if(data.success == true)
          {
            $('.username_require').html(data.message);
            $('#username').addClass('prevent_user');
          }
          else
          {
            $('.username_require').html('');
            $('#username').removeClass('prevent_user');
          }
        }
      });
    }
  </script>
@stop