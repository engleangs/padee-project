@extends('layouts.default')
@section('title',trans('admin_user.view_profile'))
@section('header',trans('admin_user.user_management'))
@section('breadcrumb')
 <ol class="breadcrumb">
                        <li>
                            <a href="{{url('/')}}">
                                <i class="fa fa-dashboard"></i> 
                                    {{trans('admin_dashboard.dashboard')}}
                            </a>
                        </li>
                        <li>
                            <a href="{{action('UserController@anyIndex')}}">
                                <i class="fa fa-users"></i> 
                                    {{trans('admin_user.user')}}
                            </a>
                        </li>
                        <li class="active"><i class="fa fa-eye"></i>{{trans('admin_user.view_profile')}}</li>
    </ol>
@stop
@section('content')
<div class="row">
<div class="col-md-12">
	<!-- left column -->
	<!-- general form elements -->
	 <!-- form start -->
	<form class="form-horizontal">
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <h3 class="panel-title">{{trans('admin_user.view_profile')}}</h3>
	    </div><!-- /.box-header -->
	   <div class="panel-body">
	   <table class="tbl-view">
	   	<tr>
	   		<td class="col-sm-3">
	   			{{trans('admin_user.full_name')}}
	   		</td>
	   		<td> : </td>
	   		<td class="col-sm-8"> {{$user->full_name}}</td>

	   	</tr>
	   	<tr>
	   		<td class="col-sm-3">
	   			{{trans('admin_user.email')}}
	   		</td>
	   		<td> : </td>
	   		<td class="col-sm-8">{{$user->email}}</td>
	   	</tr>
	   	<tr>
	   		<td class="col-sm-3">
	   			{{trans('admin_user.user_name')}}
	   		</td>
	   		<td> : </td>
	   		<td class="col-sm-8"> {{$user->username}} </td>
	   	</tr>
	   		   
	   </table>

		  
			
		</div>
		<div class="panel-footer">
			<div class="col-sm-5 pull-left col-sm-offset-2">
				<a  class="btn btn-primary"
						href="{{action('UserController@getEditProfile')}}" 
					 id="btn_save"> 
						<i class="fa  fa-edit"></i> {{trans('admin_default.edit')}} 
				</a>
					&nbsp;
				<div class="clearfix"></div>
			</div>				
			
			<div class="clearfix"></div>	
		</div>
	        
	    
	</div>
	</form>
</div>
</div>


@stop