<?php 
/**
 * 
 */
 class ListHelper 
 {
 	private static $instance = null;
 	private $defaultController = 'DefaultController';
 	private $defaultAction  = array(
 				'add'=>array(
 						'action'=>'getAdd',
 						'order'=>1,
 						'text'=>'Add New',
 						'icon'=>'fa fa-plus',
 						'id'=>'btn-menu-add',
 						'class'=>'',
 						),
 				'seperator'=>array(
 						'action'=>'seperator',
 						'order'=>4,
 						
 					),
 				'edit'=>array(
 						'action'=>'getEdit',
 						'order'=>2,
 						'text'=>'Edit',
 						'icon'=>'fa fa-edit',
 					),
 				/*'view'=>'getView',*/
 				'delete'=>array(
 						'action'=>'getDelete',
 						'text'=>'delete',
 						'icon'=>'fa fa-trash',
 						'order'=>3),
 				);
 	private $menus = array();
 	public function setDefaultController($controller)
 	{
 		$this->defaultController = $controller;
 	}
 	public function setAction($name,$action)
 	{
 		$this->defaultAction[$name] =$action;
 	}

 	public function remove($name)
 	{
 		if(isset($this->defaultAction[$name]))
 		{
 			unset($this->defaultAction[$name]);
 		}
 	}
 	public function getAction($name,$default=null)
 	{
 		if(isset($this->defaultAction[$name]))
 		{
 			return $this->defaultAction[$name];
 		}
 		return $default;
 	}
 	public function getDefaultController()
 	{
 		return $this->defaultController;
 	}

 	private function __construct()
 	{
 		
 	}
 	public static function getInstance()
 	{
 		if(self::$instance == null)
 		{
 			self::$instance = new ListHelper();
 		}
 		return self::$instance;
 	}
 	public function buildMoreActionMenu()
 	{
 		$data = array();
 		foreach ($this->defaultAction as $key => $value) 
 		{
 			if(!isset($data[$value['order']]))
 			{
 				$data[$value['order']] = array();
 			}
 			$data[$value['order']][] = $value;

 		
 		}
 		ksort($data);
 		
 		
 		foreach ($data as $i => $items) 
 		{
 			foreach ($items as $key => $value) 
 			{
 				

 				if($value['action']=='seperator')
 				{
 					$this->menus[]  = new Menu('seperator');
 				}
 				else
 				{
 					$this->menus[] = new Menu('link',$value['text'],$value['icon'],$this->defaultController,$value['action']);
 				}
 				
 			}
 		}

 		

 	}
 	public function renderMoreActionMenu()
 	{
 		$str = '';
 		try {
	 			foreach ($this->menus as $key => $value) 
		 		{
		 			try {
		 				$str = $str.$value;	
		 			} catch (Exception $e) 
		 			{
		 				echo $e->getMessage();
		 				exit;	
		 			}
		 		}	
		 		
 		} 
 		catch (Exception $e) 
 		{
 			echo $e->getMessage();
 			exit;
 		}
 		
 		$str = '<div class="btn-group pull-right">
	 				<span 
		                class="more-action dropdown-toggle" 
		                rel="tooltip"
		                title="More Action"
		                data-toggle="dropdown">
		                <i class="fa  fa-cog"></i>
	            	</span>
            		<ul class="dropdown-menu" role="menu">'.$str.
            	'	</ul>
            	</div>';
 		return  $str;
 	}



 	
 }
 function array_insert(&$array, $value, $index)
{
    return $array = array_merge(array_splice($array, max(0, $index - 1)), array($value), $array);
}
 ?>