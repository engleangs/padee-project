<?php 
interface AdminModel
{
	function getItem($key);
	function doPaginate($per_page,$condition,$order,$dir);
	function saveAll($key,$data);
	function state($key,$state);
	function updateStates($lst_id=array(),$state);
	function deleteList($lst_id);

} ?>