<?php 
/*************************************************
*   *File Name: Helper
*   *Functionality: For Helper Function
    *History:

        - 2015-06-01 Sim Chhayrambo Initial Version

*   *Developed & designed By : ABI-Technologies
*   
*************************************************/
 class Helper 
 {
 	public static $currentAction = null;
 	public static $moreAction = array(); 
 	private static $defaultController = 'DefaultController';
    public static $langsCodes = array();
    public static $module   = '';
 	public static function setDefaultController($controllerName)
 	{
 			self::$defaultController = $controllerName;
 	}
 	public static function  getDefaultController()
 	{
 		return self::$defaultController;
 	}

 	public static function getMoreActionUrl()
 	{
 		return self::$moreAction;
 	}
    public static function getExportButton($url)
    {
        return '<a  
                    href="'.$url.'"
                    rel="tooltip" 
                    data-toggle="tooltip" 
                    data-original-title="'.trans('admin_default.click_to_export').'"
                >
                    <i class="fa fa-file-text"></i>
                </a>&nbsp;';
    }
    public static function getSubDescription($description) 
    {
        $remove_tag = strip_tags($description);
        if(strlen($remove_tag) > 300)
        {
            return substr($remove_tag, 0 ,300)."...";
        }
        
        return $remove_tag;
    }
    public static function getSubTitle($title)
    {
        $title = strip_tags($title);
        if(strlen($title) > 16)
        {
            return substr($title, 0 ,16)."...";
        }
        return $title;
    }
    public static function getSubStr50($name)
    {
        $name = strip_tags($name);
        if(strlen($name) > 50)
        {
            return substr($name, 0 ,50)."...";
        }
        return $name;
    }
    public static function getShortDescripbe($description)
    {
        $remove_tag = strip_tags($description);
        if(strlen($remove_tag) > 130)
        {
            return substr($remove_tag, 0 ,130).'...';
        }
        
        return $remove_tag;
    }
    public static function getShortReview($review)
    {
        $remove_tag = strip_tags($review);
        if(strlen($remove_tag) > 200)
        {
            return substr($remove_tag, 0 ,200).'...';
        }
        
        return $remove_tag;
    }
    public static function getEditButton($url, $id = 0, $frontend = false)
 	{

        if(!$frontend && !self::checkModulePermission(self::$module,'can_edit')) return '';
 		return '<a href="'.$url.'" 
 					rel="tooltip"
 					class="edit-button" 
                    data-id ="'.$id.'"
 					data-toggle="tooltip" data-original-title="'.trans('admin_default.click_to_edit').'"
 				>
 					<i class="fa fa-edit"></i>
 				</a>&nbsp;';

 	}
    public static function getEditButtonFrontend($url, $id = 0, $frontend = false)
    {

        if(!$frontend && !self::checkModulePermission(self::$module,'can_edit')) return '';
        return '<a href="'.$url.'" 
                    rel="tooltip"
                    class="edit-button" 
                    data-id ="'.$id.'"
                    data-toggle="tooltip" data-original-title="'.trans('admin_default.click_to_edit').'"
                >
                    <i class="icon-edit"></i>
                </a>&nbsp;';

    }
    public static function getRenewButton($url, $id = 0, $frontend = false)
    {
        if(!$frontend && !self::checkModulePermission(self::$module,'can_edit')) return '';
        return '<a href="'.$url.'" 
                    rel="tooltip"
                    class="edit-button" 
                    data-id ="'.$id.'"
                    data-toggle="tooltip" data-original-title="'.trans('admin_default.click_to_renew').'"
                >
                    <i class="fa fa-refresh"></i>
                </a>&nbsp;';

    }
 	public static function getDeleteButton($url,$id=0, $frontend = false)
 	{ 
        if(!$frontend && !self::checkModulePermission(self::$module,'can_delete')) return '';
 		return '<a
 					class="trash-button"
 					data-id="'.$id.'" 
 					href="'.$url.'" 
 					rel="tooltip" 
                    data-url="'.$url.'" 
 					data-toggle="tooltip" data-original-title="'.trans('admin_default.click_to_delete').'"
 				>
 					<i class="fa  fa-trash-o"></i>
 				</a>&nbsp;';
 	}
    public static function getDeleteButtonFrontend($url,$id=0, $frontend = false)
    { 
        if(!$frontend && !self::checkModulePermission(self::$module,'can_delete')) return '';
        return '<a
                    class="trash-button"
                    data-id="'.$id.'" 
                    href="'.$url.'" 
                    rel="tooltip" 
                    data-url="'.$url.'" 
                    data-toggle="tooltip" data-original-title="'.trans('admin_default.click_to_delete').'"
                >
                    <i class="icon-trash"></i>
                </a>&nbsp;';
    }
    public static function getViewButtonJobOffer($url)
    {
        return '<a href="'.$url.'" 
                    rel="tooltip"
                    class="view-button"
                    data-toggle="tooltip" data-original-title="'.trans('admin_default.click_to_view').'"
                >
                    <i class="fa  fa-eye"></i>
                </a>&nbsp;';
    }
 	public static function getViewButton($url)
 	{
 		if(!self::checkModulePermission(self::$module,'can_view')) return '';
        return '<a href="'.$url.'" 
 					rel="tooltip"
 					class="view-button"
 					data-toggle="tooltip" data-original-title="'.trans('admin_default.click_to_view').'"
 				>
 					<i class="fa  fa-eye"></i>
 				</a>&nbsp;';
 	}
 	public static function getCheckAllButton()
 	{
 		return '<input type="checkbox" class="btn-check-all simple" 
 					rel="tooltip"
 					id="btn-check-all"
 					data-toggle="tooltip" data-original-title="'.trans('admin_default.check_all').'" >';
 	}
   	public static function getCheckButton($id)
 	{
 		return '<input type="checkbox" name="ckb[]" class="ckb simple" value="'.$id.'" id="cb'.$id.'" />';
 	}
    public static function getCheckAllPermission($id){
        return '<input type="checkbox" class="btn-check-permission simple" 
                    rel="tooltip"
                    id="btn-check-permission-'.$id.'" 
                    class="ckb btn-check-permisstion" 
                    value="cd'.$id.'" 
                    data-toggle="tooltip" data-original-title="'.trans('admin_default.check_all_permission').'" >';
    }
    public static function getCheckAll(){
        return '<input type="checkbox" name="ckb" class="ckb simple check_all" data-toggle="tooltip" data-original-title="'.trans('admin_default.check_all').'" />';
    }
    public static function getCheckPermission($id, $label,$b=false)
    {
        $checked = '';
        if($b)
        {
            $checked ='checked="checked"';
        }
        return '<input type="checkbox" name="permission['.$id.']['.$label.']" class="ckb simple cd'.$id.'" value="1" id="cd'.$id.'" '.$checked.' data-toggle="tooltip" data-original-title="'.trans('admin_user_permission.'.$label).'"  />';
    }
 	public static function getFlashMessage()
 	{
 		$class = 'alert';
 		if(Session::has('message'))
 		{
 			$message = Session::get('message');
 			$prefix = '<strong>'.trans('admin_default.warning').'</strong>';
 			if($message['success'])
 			{
 				$class.=' alert-success';
 				$prefix = '<strong>'.trans('admin_default.info').'</strong>';
 			}
 			else{
 				// $class.=' alert-warning';
                $class.=' alert-danger';
 			}
 			return 
	 			'<div class="'.$class.'">
	        		<a href="#" class="close" data-dismiss="alert">&times;</a>
	        	'.$prefix.' '.$message['message'].'
	    		</div>';
 		}
 		return '';
 	}
 	public static function getMenuActiveClass($param)
 	{
 		if(self::$currentAction ==null)
 		{
 			self::$currentAction  = Route::currentRouteAction();
 		}
 		if(is_array($param))
 		{
 			if(in_array(self::$currentAction, $param))
 			{
 				return 'active';
 			}
 		}
 		elseif(self::$currentAction == $param)
 		{
 			return 'active';
 		}

 		return '';
 	}
    
    public static function getIsUrgentButton($state,$url,$permission=true)
    {
        $url =  $url;
        $state = (int)$state;
        if(!$permission)$url = '#';
        $button = array(
                0=>'<a 
                        data-toggle="tooltip" 
                        data-original-title="'.trans('admin_default.click_to_set_urgent').'"
                        href="'.$url.'" class="btn btn-xs btn-success deactive_button">
                            <i class="fa fa-check"></i>&nbsp;'. trans('admin_default.normal').'
                        </a>',
                1=>'<a 
                    data-toggle="tooltip" 
                    data-original-title="'.trans('admin_default.click_to_set_normal').'"
                         href="'.$url.'" class="btn btn-xs btn-danger active_button">
                    <i class="fa fa-check"></i>&nbsp;'. trans('admin_default.urgent').'</a>',
            );
        if(!$permission)
        {
            $button = array(
                        0=>'<span class="label label-success"><i class="fa fa-check"></i>&nbsp;'
                            .trans('admin_default.normal')
                            .'</span>',
                        1=>'<span class="label label-danger"><i class="fa fa-check"></i>&nbsp;'
                            .trans('admin_default.urgent')
                            .'</span>',
                );
        }
        if(isset($button[$state]))
        {
            return $button[ $state];
        }
        return '';
    }
    public static function getStateTaskButton($state,$url,$permission=true)
    {
        $url =  $url;
        $state = (int)$state;
        if(!$permission)$url = '#';
        $button = array(
                0=>'<a 
                        data-toggle="tooltip" 
                        data-original-title="'.trans('admin_default.click_to_completed').'"
                        href="'.$url.'" class="btn btn-xs btn-status deactive_button">
                            <i class="fa fa-times"></i>&nbsp;'. trans('admin_default.not_completed').'
                        </a>',
                1=>'<a 
                    data-toggle="tooltip" 
                    data-original-title="'.trans('admin_default.click_to_not_completed').'"
                         href="'.$url.'" class="btn btn-xs btn-primary active_button">
                    <i class="fa fa-check"></i>&nbsp;'. trans('admin_default.completed').'</a>',
            );
        if(!$permission)
        {
            $button = array(
                        0=>'<span class="label label-warning"><i class="fa fa-times"></i>&nbsp;'
                            . trans('admin_default.not_completed')
                            .' </span>',
                        1=>'<span class="label label-primary"> <i class="fa fa-check"></i>&nbsp;'
                            .trans('admin_default.completed')
                            .'</span>',
                );
        }
        if(isset($button[$state]))
        {
            return $button[ $state];
        }
        return '';
    }
 	public static function getStateButton($state,$url,$permission=true)
 	{
 		$url =  $url;
        $state = (int)$state;
 		if(!$permission)$url = '#';
 		$button = array(
 				0=>'<a 
 						data-toggle="tooltip" 
 						data-original-title="'.trans('admin_default.click_to_activate').'"
 					 	href="'.$url.'" class="btn btn-xs btn-status deactive_button">
 					 		<i class="fa fa-times"></i>&nbsp;'. trans('admin_default.inactive').'
 					 	</a>',
 				1=>'<a 
 					data-toggle="tooltip" 
 					data-original-title="'.trans('admin_default.click_to_deactivate').'"
 						 href="'.$url.'" class="btn btn-xs btn-primary active_button">
 					<i class="fa fa-check"></i>&nbsp;'. trans('admin_default.active').'</a>',
 			);
        if(!$permission)
        {
            $button = array(
                        0=>'<span class="label label-warning">'
                            . trans('admin_default.inactive')
                            .' </span',
                        1=>'<span class="label label-primary">'
                            .trans('admin_default.active')
                            .'</span>',
                );
        }
 		if(isset($button[$state]))
 		{
 			return $button[ $state];
 		}
 		return '';
 	}
    public static function getStatusButton($state,$url,$permission=true)
    {
        $url =  $url;
        $state = (int)$state;
        if(!$permission)$url = '#';
        $button = array(
                0=>'<a 
                        data-toggle="tooltip" 
                        data-url="'.$url.'" 
                        data-id="'.$state.'"
                        data-original-title="'.trans('admin_default.click_to_completed_call').'"
                        href="'.$url.'" class="btn btn-xs btn-status deactive_button">
                            <i class="fa fa-times"></i>&nbsp;'. trans('admin_default.schedule_call').'
                        </a>',
                1=>'<a 
                    data-toggle="tooltip" 
                    data-url="'.$url.'" 
                    data-id="'.$state.'"
                    data-original-title="'.trans('admin_default.click_to_schedule_call').'"
                         href="'.$url.'" class="btn btn-xs btn-primary active_button">
                    <i class="fa fa-check"></i>&nbsp;'. trans('admin_default.completed_call').'</a>',
            );
        if(!$permission)
        {
            $button = array(
                        0=>'<span class="label label-warning"><i class="fa fa-times"></i>&nbsp;'
                            . trans('admin_default.schedule_call')
                            .' </span',
                        1=>'<span class="label label-primary"><i class="fa fa-check"></i>&nbsp;'
                            .trans('admin_default.completed_call')
                            .'</span>',
                );
        }
        if(isset($button[$state]))
        {
            return $button[ $state];
        }
        return '';
    }
    public static function getPendingButton($state,$url,$permission=true)
    {
        $url =  $url;
        $state = (int)$state;
        if(!$permission)$url = '#';
        $button = array(
                0=>'<a 
                        data-toggle="tooltip" 
                        data-original-title="'.trans('admin_default.click_to_activate').'"
                        href="'.$url.'" class="btn btn-xs btn-status deactive_button">
                            <i class="fa fa-times"></i>&nbsp;'. trans('admin_default.pending').'
                        </a>',
                1=>'<a 
                    data-toggle="tooltip" 
                    data-original-title="'.trans('admin_default.click_to_deactivate').'"
                         href="'.$url.'" class="btn btn-xs btn-primary active_button">
                    <i class="fa fa-check"></i>&nbsp;'. trans('admin_default.active').'</a>',
            );
        if(!$permission)
        {
            $button = array(
                        0=>'<span class="label label-warning">'
                            . trans('admin_default.inactive')
                            .' </span',
                        1=>'<span class="label label-primary">'
                            .trans('admin_default.active')
                            .'</span>',
                );
        }
        if(isset($button[$state]))
        {
            return $button[ $state];
        }
        return '';
    }
 	public static function getDefault($url,$is_default=false)
 	{
 		$obj = (int)$is_default;
 		$button = array(
			0=>'<a 
					data-toggle="tooltip" 
					data-original-title="Click To Set As Default"
				 	href="'.$url.'" class="btn btn-xs btn-status">
				 		<i class="fa   fa-ban"></i> 
				 	</a>',
			1=>'<a 
				
					 href="'.$url.'" class="btn btn-xs btn-primary">
				<i class="fa fa-check"></i></a>',
 			);
 		return $button[$obj];
 	}
 	public static function getRecordNumber($selected=10,$name="per_page",$option=array())
	{
		$list = array(
                
                10=>"10",
                25=>"25",
                50=>"50",
                75=>"75",
                100=>"100",
                );
        if(!$selected) $selected = 10;
        
		$st 	= '';
        $class = isset($option['class'])?$option['class']:'';
        $class = $class." cmb-per-page  col-sm-4 width_100";
        $option['class']    = $class;
        return Form::select($name,$list, $selected,$option);		
	}
    public static function getRecordNumberLayout($selected=1,$name="per_page",$option=array())
    {
        $list = array(
               
                10=>"10",
                25=>"25",
                50=>"50",
                75=>"75",
                100=>"100",
                );
        if(!$selected) $selected = 1;
        
        $st     = '';
        $class = isset($option['class'])?$option['class']:'';
        $class = $class." col-sm-12 cmb-per-page width_100  ";
        $option['class']    = $class;
        return Form::select($name,$list, $selected,$option);        
    }
	public static function buildAdminMenu($menu_name = 'adminmenu.php')
    {
        $menu = include $menu_name;
       
        echo "<ul class='sidebar-menu'>\n";
        self::displayMenu($menu);
        echo "</ul>";
        
    }
    public static function buildUserMenu($menu_name = 'usermenu.php')
    {
        $menu = include $menu_name;
       
        echo "<ul class='nav'>\n";
            self::displayMenu($menu);
        echo "</ul>";
        
    }
    public static function getSeperator()
    {
    	return "<li class='seperator'></li>\n";
    }
    public static function getMenuItem($item)
    {
    		$class = "";
            $suffix = '';
    		if(isset($item['class'])) $class = $class." ".$item['class'];
    		if(isset($item['hasChild']) && $item['hasChild'])
    		{
    			$class.=" dropdown ";
                $suffix= '<b class="caret"></b>';

    			foreach ($item['children'] as $key => $value) 
    			{
    				$class.=Helper::getMenuActiveClass($value['url']);
    			}
                if(isset($item['active_url'])&&count($item['active_url']))
                {
                    foreach ($item['active_url'] as $i => $url) 
                    {
                        $class.=Helper::getMenuActiveClass($url);   
                    }
                }
    		}
    		else
    		{
    			$class.=Helper::getMenuActiveClass($item['url']);
    		}
    		$icon = "<i class='".$item['icon']."'></i>";
            $url = ($item['url']=='#')?'#':action($item['url']);
            $url_param  = '';
            if(isset($item['url_param'])) $url_param = $item['url_param'];
    		$text   = "<a href='".$url.$url_param."'"."class=".'"dropdown-toggle"'."data-toggle=".'"dropdown"'.">".$icon .$item['text']." $suffix"."</a>";
    		return  "<li class='$class'> $text" ;
    }
    public static function checkMenuPermission($permission)
    {
        $can_display = false;
        $user_permission = Session::get('user_permission',array()); 
        $is_super_admin = Session::get('is_super_admin',false); 
        if($is_super_admin)
        {
            return true;
        }
        if($permission['type']=='group')
        {
            $modules = $permission['modules'];
            foreach ($modules as $key => $value) 
            {   
                $tmp_permission = array();
                if(isset($user_permission[$key]))
                {
                    $tmp_permission = $user_permission[$key];
                }          
                if(is_array($value))
                {
                    foreach ($value as $action_name) 
                    {
                        if( (isset($tmp_permission[$action_name]) && $tmp_permission[$action_name]) || $is_super_admin )
                        {
                            $can_display = true;  
                            break;
                        } 
                        
                    }
                }
                else
                {
                    if(isset($tmp_permission['none_all']) && !$tmp_permission['none_all'])
                    {
                        $can_display = true;
                        break;
                    }
                }
            }
        }
        elseif($permission['type']=='groups')
        {
            $modules = $permission['module'];
            $tmp_permission = $permission['action'];
            foreach($modules as $key => $value)
            { 
                foreach($tmp_permission as $action)
                {
                    if( (isset($user_permission[$value][$action]) && $user_permission[$value][$action]) || $is_super_admin )
                    {
                        $can_display = true;
                        break;
                    }
                }
            }
        }
        elseif($permission['type']=='single')
        {
            $module     = $permission['module'];
            $action     = $permission['action'];
            $tmp_permission = array();
            if(is_array($action))
            {
                foreach($action as $item)
                {
                    if( (isset($user_permission[$module][$item]) && $user_permission[$module][$item]) || $is_super_admin ) 
                    {
                        $can_display = true;
                        break;
                    }
                }
            }
            else
            {
                if(isset($user_permission[$module][$action]))
                {
                    $tmp_permission  = $user_permission[$module][$action];
                }
                if( (isset($tmp_permission[$module][$action]) && $tmp_permission[$module][$action]) || $is_super_admin )
                {
                    $can_display = true;
                }
            }
        }

        return $can_display;
    }
    public static function checkPagePermission($module,$action)
    {
        $user_permission = Session::get('user_permission',array()); 
        $is_super_admin = Session::get('is_super_admin',array()); 
        $display = false;
        if( (isset($user_permission[$module][$action]) && $user_permission[$module][$action]) || $is_super_admin )
        {
            $display = true;
        }
        return $display;
    }
    public static function  displayMenu(array $menu)
    { 

        // var_dump($menu);
        // exit();

    	foreach ($menu as $key => $item) 
    	{
    		switch ($item['type'])
    		{
    			case 'seperator':
	    				echo self::getSeperator();
	    				break;
	    		default:
                    $hasPermission   = true;
                    
                    if(isset($item['permission']))
                    {   
                        $permission = $item['permission'];
                        $hasPermission =  self::checkMenuPermission($permission);
                    }
                    if($hasPermission)
                    {
                        echo self::getMenuItem($item);
                            if(isset($item['hasChild']) && $item['hasChild'])
                            {
                                echo '<ul class="dropdown-menu">';
                                echo "\n";
                                self::displayChildMenu($item['children']);
                                echo "</ul>\n";
                            }
                         echo "</li>\n";
                    }

	    			break;
    		}
    		
    	}
    }

    public static function getChildMenuItem($item){
        $class = "";
            $suffix = '';
            if(isset($item['class'])) $class = $class." ".$item['class'];
            if(isset($item['hasChild']) && $item['hasChild'])
            {
                $class.=" dropdown ";
                $suffix= '<b class="caret"></b>';

                foreach ($item['children'] as $key => $value) 
                {
                    $class.=Helper::getMenuActiveClass($value['url']);
                }
                if(isset($item['active_url'])&&count($item['active_url']))
                {
                    foreach ($item['active_url'] as $i => $url) 
                    {
                        $class.=Helper::getMenuActiveClass($url);   
                    }
                }
            }
            else
            {
                $class.=Helper::getMenuActiveClass($item['url']);
            }
            $icon = "<i class='".$item['icon']."'></i>";
            $url = ($item['url']=='#')?'#':action($item['url']);
            $url_param  = '';
            if(isset($item['url_param'])) $url_param = $item['url_param'];
            $text   = "<a href=".$url.$url_param.">".$icon .$item['text']." $suffix"."</a>";
            return  "<li> $text" ;
    }

    public static function displayChildMenu(array $menu){
        foreach ($menu as $key => $item) 
        {
            switch ($item['type'])
            {
                case 'seperator':
                        echo self::getSeperator();
                        break;
                default:
                    $hasPermission   = true;
                    
                    if(isset($item['permission']))
                    {   
                        $permission = $item['permission'];
                        $hasPermission =  self::checkMenuPermission($permission);
                    }
                    if($hasPermission)
                    {
                        echo self::getChildMenuItem($item);
                            if(isset($item['hasChild']) && $item['hasChild'])
                            {
                                echo '<ul class="dropdown-menu">';
                                echo '<li></li>';
                                echo "\n";
                                self::displayMenu($item['children']);
                                echo "</ul>\n";
                            }
                         echo "</li>\n";
                    }

                    break;
            }
            
        }
    }

    static function getLangId()
    {
        if(!Session::has('app.lang_list') || true)
        {
            $data = DB::table('languages')
                    ->select('*')->get();  
            $lst    = array();
            self::$langsCodes = array();
            foreach ($data as $key => $obj) 
            {
                  $lst[$obj->id]  = $obj;
                  self::$langsCodes[$obj->code] = $obj->id;
                  if($obj->is_default)Session::set('app.lang_default_id',$obj->id);
            }  
            Session::set('app.lang_list',$lst);            
        }

        $obj =  Session::get('app.lang_list', array());
        return $obj;
    }
    public static function getCurrentLangId()
    {
        $code = Config::get('app.local_prefix', 'en');
        self::getLangId();
        if(isset(self::$langsCodes[$code])) return self::$langsCodes[$code];
        return 1;
    }
    public static function getDefaultLangId()
    {
        if(!Session::has('app.lang_default_id'))
        {
            self::getLangId();
        }
        return Session::get('app.lang_default_id', 1);
    }
    public static function getDefaultLang()
    {
        $langs = self::getLangId();
        $langCode = Cookie::get('app_lang', 'en');
        if(!$langCode) $langCode = 'en';
        foreach ($langs as $key => $value) 
        {
            if($value->code == $langCode)
            return $value; 
        }
        return null;
    }
    public static function getDateFormat($date)
    {
        return date('m/d/Y H:i:s',strtotime($date));
    }
    // format number
    public static function getNumberFormat($number){
        return number_format($number);
    }
    // format phone number
    public static function getPhoneNumberFormat($phone_number){
        if(  preg_match( '/^\+\d(\d{3})(\d{3})(\d{4})$/', $phone_number,  $matches ) )
        {
            $result = $matches[1] . '-' .$matches[2] . '-' . $matches[3];
            return $result;
        }
        return $phone_number;
    }
    public static  function seoUrl($string) 
    {
        //Lower case everything
        $string = strtolower($string);
        //Make alphanumeric (removes all other characters)
        $string = str_replace  ("'", "", $string);
        $string = preg_replace ('/[^\p{L}\p{N}]/u', '-', $string);
        /*$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);*/
        //Clean up multiple dashes or whitespaces
        $string = preg_replace("/[\s-]+/", " ", $string);
        //Convert whitespaces and underscore to dash
        $string = preg_replace("/[\s_]/", "-", $string);
        return $string;
    }
    public static function getLocation()
    {
        $location = include 'AdminLocation.php';
        
        return $location;
    }
    public static function checkModulePermission($module,$action = array())
    {
        $user_permission = Session::get('user_permission',array()); 
        $is_super_admin = Session::get('is_super_admin',array()); 
        $display = false;
        if(is_array($action) && count($action))
        {
            foreach ($action as $key => $value) 
            {  
                if( (isset($user_permission[$module][$value]) && $user_permission[$module][$value]) || $is_super_admin )
                {
                    $display = true;
                }
            }
        }
        else
        {
            if( (isset($user_permission[$module][$action]) && $user_permission[$module][$action]) || $is_super_admin )
                {
                    $display = true;
                }

        }
        return $display;
    }
    public static function getIsLogIn($data = array())
    {
        $is_login = false;
        if( count($data) > 0 )
        {
            $is_login = true;
        }

        return $is_login;
    }
}


 ?>
