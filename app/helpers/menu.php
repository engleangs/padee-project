<?php /**
* 
*/
class Menu 
{
	public $action;
	public $controller;
	public $icon;
	public $tag='li';
	public $children = array();
	public $totalChildren = 0;
	public $text = '';
	public $type = 'link';
	public $level = 1;
	public $parent_id = 0;
	public $lft = 0;
	public $rght = 0;
	function __construct($type="",$text="",$icon="",$controller="",$action="",$tag='li',$level=1,$parent_id=0)
	{
		$this->type=$type;
		$this->text = $text;
		$this->icon = $icon;
		$this->controller = $controller;
		$this->action = $action;
		$this->tag  = $tag;
		$this->level = $level;
		$this->parent_id = $parent_id;
	}
	public function __toString()
	{
		$str = '';
		try {
			switch ($this->type) 
			{
				default:
				$action = '#';
				if($this->action!='#')
				{
					$action =  action($this->controller.'@'.$this->action);
				}
				$str =  "<".$this->tag.'>'.
						'<a href="'.$action.'">'
							.'<i class="fa '.$this->icon.'"></i>'.
							$this->text.
						'</a>'.
						'</'.$this->tag.'>';
					
					break;
				case 'seperator':
					$str = '<'.$this->tag.' class="divider"><'.$this->tag.'>';
					break;
				
				
			}
			
		} catch (Exception $e) {
			echo "Error : ". $e->getMessage();
			var_dump($this);
			exit;
		}
		
		
		return $str;
	}
} ?>