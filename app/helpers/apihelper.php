<?php
/**
 * @Author: Engleang SAM
 * @Date:   2014-11-14 19:33:59
 * @Last Modified by:   user
 * @Last Modified time: 2014-12-12 16:56:06
 */
function mytest()
{
	
	echo "test";
}

function langId()
{
    return Helper::getDefaultLangId();
}

function bind_dealer_logo($dealer)
{

	$dealer->logo = get_dealer_logo($dealer->logo);
	return $dealer;
}

function replace_landscape_image($vehicle)
{
	$landscape_image = asset('resources/vehicle/200_200_m/default-logo.png');
	$vehicle->thumbnail_landscape_image = get_landscape_thumbnail($vehicle->landscape_image);

	if(isset($vehicle->landscape_image))
	{
		$path  		= "resources/vehicle/200_200_m/{$vehicle->landscape_image}";
		$full_path 	= public_path()."/$path";
		if(file_exists($full_path) && !is_dir($full_path))
		{
			$landscape_image = asset($path);
			$vehicle->thumbnail_landscape_image = get_landscape_thumbnail($vehicle->landscape_image);
		}
		$vehicle->landscape_image = $landscape_image;
		

	}

	return $vehicle;
}

function replace_feature_image($vehicle)
{
	$vehicle->thumbnail_feature_image = "";	
	if(isset($vehicle->feature_image))
	{	
		$vehicle->thumbnail_feature_image = get_feature_thumbnail($vehicle->feature_image);
		$vehicle->feature_image 	= get_feature_vehicle_image($vehicle->feature_image);
		
	}	
	return $vehicle;
}

function get_ads_image($image)
{
	$path 	= "resources/ads/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))	
	{
		return asset($path);
	}
	return "";
}
function get_feature_thumbnail($image)
{
	$feature_image = asset('resources/vehicle/200_200_m/default-logo.png');	
	$path  		   	= "resources/vehicle/200_200_m/$image";
	$full_path 		= public_path()."/$path";		
	if(file_exists($full_path) && !is_dir($full_path))
	{
		$feature_image = asset($path);
	}
		
	return $feature_image;	
}
function get_landscape_thumbnail($image)
{
	$feature_image = asset('resources/vehicle/200_200_m/default-logo.png');	
	$path  		   	= "resources/vehicle/200_200_m/$image";
	$full_path 		= public_path()."/$path";		
	if(file_exists($full_path) && !is_dir($full_path))
	{
		$feature_image = asset($path);
	}
		
	return $feature_image;	
}
function get_category_logo($image)
{
	$path = "resources/businesscategory/200_200/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/businesscategory/200_200/default-logo.png');
}
function get_property_logo($image)
{
	$path = "resources/property/960_550/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}

	return asset('resources/default-logo.png');
}
function get_property_gallery_image($image)
{
	$path = "resources/property/960_550/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/property/no-image.png');
}
function get_agent_image($image)
{
	$path = "resources/agent/200_200/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/no-image.png');
}
function get_event_image($image)
{
	$path = "resources/event/600_500/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/no-image.png');
}
function get_office_logo($image)
{
	$path = "resources/branch/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/no-image.png');
}

// property images
function get_property_image($image)
{
	$path = "resources/property/250_143/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/property/no-image.png');
}
function get_property_image_500_286($image)
{
	$path = "resources/property/500_286/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/property/no-image.png');
}
function get_property_image_960_550($image)
{
	$path = "resources/property/960_550/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/property/no-image.png');
}
// end property images

// company profile
function get_profile_image($image)
{
	$path = "resources/profile/300_200/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/profile/default.png');
}
// 

// 2015-07-13
function get_slideshow_images($image)
{
	$path = "resources/slideshow/950_362/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/no-image.png');
}
function get_slideshow_images_1300_495($image)
{
	$path = "resources/slideshow/1300_495/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/no-image.png');
}
function get_slideshow_images_1970_750($image)
{
	$path = "resources/slideshow/1970_750/$image";
	$full_path = public_path()."/$path";
	if(file_exists($full_path) && !is_dir($full_path))
	{
		return asset($path);
	}
	return asset('resources/no-image.png');
}