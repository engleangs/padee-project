<?php
/*************************************************
*	*File Name: User Model
*	*Functionality:
	*History:

		- 2015-06-01 Sim Chhayrambo Initial Version

*	*Developed & designed By : ABI-Technologies
*	
*************************************************/

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	
	public function doPaginate($per_page,$condition,$order,$dir)
	{
		$query = $this->select(
						array(
							'users.*',
							'user_roles.name'
						)
					);
		$query->join('user_roles','user_roles.id','=','users.user_role');
		if(count($condition))
		{
			if(isset($condition['search']))
			{
				$search = (string)$condition['search'];
				$query->where(function($query) use($search)
				{
					$query->where('username','LIKE',"%$search%")
						  ->orWhere('full_name','LIKE',"%$search%")
						  ->orWhere('email','LIKE',"%$search%");
				});
			}
			if(isset($condition['user_id']))
			{
				$query->where('users.id','<>',$condition['user_id']);
			}
			if(isset($condition['status']) )
				{
					$status=$condition['status'];
					if($status !=='')
					{
						$query->where('users.state','=',$status);	
					}						
				}
		}
		$query->where('users.is_super_admin','=',0);
		return $query->paginate($per_page);
	}
	public function getItem($key)
	{
		return $this->select('users.*','user_roles.name as role')
				->leftJoin('user_roles','user_roles.id','=','users.user_role')
				->where('users.id','=',$key)->get();
	}
	// public function saveAll($key,$data)
	// {
	// 	if(!$key)
	// 	{
	// 		$data['created_at'] = date('Y-m-d H:i:s');
	// 		$data['updated_at'] = date('Y-m-d H:i:s');
	// 		return User::insert($data);
	// 	}
	// 	else{
	// 		$data['updated_at'] = date('Y-m-d H:i:s');
	// 		return User::whereId($key)->update($data);
	// 	}
	// }
	function saveAll($key,$data)
	{
		$user = Auth::users()->get();
		if($key)
		{
			$this->id = $key;	
			$this->exists = true;
			$this->updated_by = $user->id;
		}
		else{
				$this->created_by = $user->id;	
		}
		foreach ($data as $field => $value) 
		{
			$this->$field = $value;
		}
		$this->save();
		$key = $this->id ;
		return $key;
	}

	public function scopeGetCountUsername($query,$username,$id)
	{
		
		$lst = $query->select(DB::raw('count(*) as total'))
			 ->where('username','=',$username)
			 ->where('id','<>',$id)
			 ->get();
		return $lst;		
		
		
	}
	public function scopeGetCountEmail($query,$email,$id)
	{
		
		$lst = $query->select(DB::raw('count(*) as total'))
			 ->where('email','=',$email)
			 ->where('id','<>',$id)
			 ->get();
		return $lst;

	}

	public function state($key,$state)
	{
		return $this->where('id',$key)
			 ->update(array('state'=>$state));
	}
	
	public function updateStates($lst_ids=array(),$state)
	{
	 	return $this->whereIn('id',$lst_ids)->update(array('state'=>$state));
	}
	public function deleteList($lst_ids)
	{

	}
	public function scopeGetUserNames($query)
	{
		$lst 		= array();
		$data 		= $query->select('*')->where('users.is_super_admin','=',0)->get();
		foreach ($data as $key => $obj) 
		{
			$lst[$obj->id] = $obj->full_name;
		}
		return $lst;
	}
	public function getPermission($id)
	{
		$query = DB::table('users');
		$query->select(
					array(
						'user_permissions.can_add',
						'user_permissions.can_view',
						'user_permissions.can_edit',
						'user_permissions.can_delete',
						'user_permissions.none_all',
						'permissions.add',
						'permissions.view',
						'permissions.edit',
						'permissions.delete',
						'permissions.module_name'
					)
				);
		$query->join('user_permissions', function($join){
					$join->on('user_permissions.role_id','=','users.user_role');
				})
			  ->join('permissions',function($join){
			  		$join->on('user_permissions.permission_id','=','permissions.id');
			  });
		$query->where('users.id','=',$id)
			  ->where('users.state','=',1);
		$data = $this->mergeData($query->get());
		return $data;
	}
	public function getSpecialPermission($id)
	{
		$query = DB::table('users');
		
		$query->select(
					array(
						'special_permissions.can_add',
						'special_permissions.can_view',
						'special_permissions.can_edit',
						'special_permissions.can_delete',
						'special_permissions.none_all',
						'permissions.add',
						'permissions.view',
						'permissions.edit',
						'permissions.delete',
						'permissions.module_name',
						'users.*'
					)
				);
		$query->join('special_permissions', function($join){
					$join->on('special_permissions.user_id','=','users.id');
				})
			  ->join('permissions',function($join){
			  		$join->on('special_permissions.permission_id','=','permissions.id');
			  });
		$query->where('users.id','=',$id)
			  ->where('users.state','=',1);
		$data = $this->mergeData($query->get());
		return $data;
	}
	public function mergeData($items){
		$data = array();
		$module_name = "";
		foreach($items as $key=>$item){
			$module_name = $item->module_name;
			unset($item->module_name);
			foreach($item as $id=>$value){
				//if($value)
					$data[$module_name][$id] = $value;
			}
		}
		return $data;
	}

}
