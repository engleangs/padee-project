<?php
	class Indicator extends Eloquent implements AdminModel
	{
		protected $table='indicators';
		protected $guarded = array();
		public function getAboutUs()
		{
			return $data = DB::table('indicators')->first();
		}
		public function doPaginate($per_page,$condition,$order,$dir)
		{
			$query = $this->select('*');
			$dir = $dir=="ASC"?$dir:"DESC";
			if(!$order) $order = 'indicators.created_at';
			$query->orderBy($order,$dir);
			if(count($condition))
			{
				
				if(isset($condition['search']))
				{
					$search = (string)$condition['search'];
					$query->where(function($query) use($search)
					{
						$query->where('title','LIKE',"%$search%");
					});
				}
			}
			return $query->paginate($per_page);
		}
		function getItem($key)
		{
		  	return \Indicator::find($key);
		}
	  	function saveAll($key,$data)
	  	{
	  		if($key)
	  		{
	  			\Indicator::whereId($key)->update($data);
	  		}
	  		else
	  		{
	  			$contact = new \Indicator($data);
	  			$contact->save();		
	  			return $contact->id;
	  		}
	  		return $key;

	  	}
		function state($key,$state)
		{

		}
		function updateStates($lst_id=array(),$state)
			{

		}
		function deleteList($lst_id)
		{

		}
	}
?>