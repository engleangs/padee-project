<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//

});


App::after(function($request, $response)
{
	//
	
});
/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});
Route::filter('lang_handler', function()
{
	$lang_code 	= Input::get('lang', null);
	$local 		= array('en','kh');	
	if(!is_null($lang_code) && in_array($lang_code, $local))
	{
		$uri = Request::segments();
		$str_prefix = $lang_code;
		if(count($uri) && in_array($uri[0],$local))
		{
			$str_prefix = "";
			$uri[0] = $lang_code;

		}
		
		$cookies = Cookie::make('local_prefix', $lang_code, 1000);
		$url = URL::to("/");
		$length = strlen($url);

		if($length>0 &&$url[$length-1] !='/' && $str_prefix!='')
		{
			$url = $url.'/';
		}
		$url = $url.$str_prefix;		
		if(count($uri))
		{
			$url  = $url."/".implode("/", $uri);
		}
		
		 return Redirect::to($url)
		 		->withCookie($cookies);

	}
	if(!Auth::users()->check())
		{
			$url = Request::url();
			Session::set('last_url', $url);
			return Redirect::to(action('UserController@getLogin'));
		 }
		


});