-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 16, 2015 at 12:22 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `padee`
--

-- --------------------------------------------------------

--
-- Table structure for table `indicators`
--

DROP TABLE IF EXISTS `indicators`;
CREATE TABLE IF NOT EXISTS `indicators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `indicator_id` varchar(15) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `group` int(11) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `component` tinyint(1) unsigned NOT NULL,
  `baseline_value` varchar(15) NOT NULL,
  `baseline_year` date NOT NULL,
  `target_value` varchar(15) NOT NULL,
  `target_year` date NOT NULL,
  `description` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `indicators`
--

INSERT INTO `indicators` (`id`, `indicator_id`, `title`, `group`, `type`, `component`, `baseline_value`, `baseline_year`, `target_value`, `target_year`, `description`, `state`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'ABC001', 'Indicator 001', 1, 1, 1, '0', '2015-08-01', '100', '2015-08-31', 'This is a description for indicator 001', 1, '2015-08-27 00:00:00', '2015-08-27 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `flag` varchar(300) DEFAULT NULL,
  `code` char(3) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `NewIndex1` (`code`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `flag`, `code`, `is_default`) VALUES
(1, 'en', 'english.png', 'en', 1),
(2, 'ážáŸ’áž˜áŸ‚ážš', 'cambodia.png', 'kh', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `id` varchar(255) NOT NULL,
  `payload` text NOT NULL,
  `last_activity` int(11) NOT NULL,
  UNIQUE KEY `sessions_id_unique` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`id`, `payload`, `last_activity`) VALUES
('a28508496dfa038b2e5a5be5c67a51dbbd0adddd', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiQWFlMVZsSDFXaDc5bUdUU1ZnYzRlVFBnU1Fmc2V3ZDRWUFVsd2FGYiI7czo4OiJsYXN0X3VybCI7czozNToiaHR0cDovL2xvY2FsaG9zdC9wYWRlZV9jbG9uZS9wdWJsaWMiO3M6NToiZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo0NDoibG9naW5fdXNlcnNfNWFiMTgzN2U5MTI0NWJmNTEzNDQyYzcwOGI1MDI1NGQiO2k6MTtzOjE0OiJpc19zdXBlcl9hZG1pbiI7aToxO3M6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNDQ0OTkwMDU4O3M6MToiYyI7aToxNDQ0OTkwMDIyO3M6MToibCI7czoxOiIwIjt9fQ==', 1444990058),
('eb3c148111380e53e26f7bbd5a98cb8472d79e1b', 'YTo2OntzOjY6Il90b2tlbiI7czo0MDoiazh3dEZUdDdtNEpMQXFLOVNKM2lLcXE1ZFRCYmdPWkdGNER3Umw5dyI7czo4OiJsYXN0X3VybCI7czoyOToiaHR0cDovL2xvY2FsaG9zdC9wYWRlZS9wdWJsaWMiO3M6NToiZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo0NDoibG9naW5fdXNlcnNfNWFiMTgzN2U5MTI0NWJmNTEzNDQyYzcwOGI1MDI1NGQiO2k6MTtzOjE0OiJpc19zdXBlcl9hZG1pbiI7aToxO3M6OToiX3NmMl9tZXRhIjthOjM6e3M6MToidSI7aToxNDQwNjcyNzEwO3M6MToiYyI7aToxNDQwNjYwNTM5O3M6MToibCI7czoxOiIwIjt9fQ==', 1440672710);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `user_role` varchar(255) DEFAULT NULL,
  `is_super_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `state` tinyint(1) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `remember_token` mediumtext,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `full_name`, `email`, `username`, `password`, `is_deleted`, `user_role`, `is_super_admin`, `state`, `created_at`, `updated_at`, `created_by`, `updated_by`, `remember_token`) VALUES
(1, 'Super Admin', 'superadmin@email.com', 'admin', '$2y$10$Tbamh8X2PTZ3MRQRMioYq.GiZdG92e9nHMXqIjLLN12CE/mjMgpqu', 0, '1', 1, 1, '2014-12-23 10:50:20', '2015-08-26 16:28:02', NULL, NULL, 'HLkUaodGF72TAvHrruwC49ZBZzRSPAZZsRwCxZhlCMjfLlS5XpwBGehiuKdM');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
