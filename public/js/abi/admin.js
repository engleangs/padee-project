if(typeof locale =='undefined')
{
	locale = 'en';	
}
window.ParsleyValidator.setLocale(locale);
var $lst_block = [];
var validateFormCallBack = function(formInstance)
{
	for(i=0;i<$lst_block.length;i++)
		{
			var blockName = $lst_block[i];
			if(!formInstance.isValid(blockName,true))
			{
				
				$('a[href="#'+blockName+'"]').tab('show');
				formInstance.submitEvent.preventDefault();
				return ;
			}
		}
		
};
function label(var_name,default_val)
{
	try{
		str = eval(var_name);
		return str;
	}catch (e) 
	{
		return default_val;
	}
}
function show_notification(message) {
	alertify.set({
		delay : 2500,
		buttonReverse : false,
		buttonFocus   : "ok"
	});
	alertify.log(message);
}
function changeGalleryLimit(limit)
{
	var old_limit = $('.box-gallery').attr('data-limit');
	var Imglength  = $('.box-gallery').find('img.img-thumbnail').length;
	var boxLength = $('.box-gallery').find('.img-wrapper').length;

	if(Imglength==boxLength && Imglength<limit)
	{
		var cloneLast = $('.box-gallery .img-gallery:last').clone(true);
		cloneLast.find('img.img-thumbnail').remove();

		cloneLast.find('.btn-add-gallery').show();
		var count = parseInt(cloneLast.find('.btn-add-gallery').attr('rel'));
		if(isNaN(count))count = 0;
		count++;
		var hidden_id = 'tmp_gallery_'+count;
		cloneLast.find('.btn-add-gallery').attr('rel',count)
											.attr('data-hidden',hidden_id)
											.before('<div class="img-thumbnail"></div>');
		cloneLast.find('input[type="hidden"]').attr('id',hidden_id);
		cloneLast.find('.btn-remove-gallery').attr('data-hidden',hidden_id);
		cloneLast.find('input[type="checkbox"]').removeAttr('checked');
		cloneLast.appendTo('.box-gallery');

	}
	if (Imglength>=limit )
	{
		show_notification('Current image gallery has reached limit. Please remove some of them !');
		if( boxLength>Imglength)
		{
			$('.box-gallery .img-gallery:last').remove();	
		}
		
	}
	
	$('.box-gallery').attr('data-limit',limit);
}
$('document').ready(function () {
	/*$('#abi-table').dataTable({
		 "bPaginate": true,
        "bLengthChange": true,
        "bFilter": false,
        "bSort": true,
        "bInfo": true,
        "bAutoWidth": false
	});*/

	$('.box-col').click(function(){
		if(!$(this).hasClass('box-toggled'))
		{
			$(this).closest('.box').find('.box-body').hide('slow');	
			$(this).addClass('box-toggled');
			$(this).find('.i-show').hide();
			$(this).find('.i-hide').show();
		}
		else
		{
			$(this).closest('.box').find('.box-body').show('slow');	
			$(this).removeClass('box-toggled');
			$(this).find('.i-show').show();
			$(this).find('.i-hide').hide();
		}	

	});

	$('.box-col').each(function(){

		if($(this).hasClass('box-toggled'))
		{
			$(this).closest('.box').find('.box-body').hide('slow');	
			$(this).addClass('box-toggled');
			$(this).find('.i-show').hide();
			$(this).find('.i-hide').show();
		}
		
	});
	$('.filter-lang,.cmb-per-page').change(function(){
		$('#adminListForm').submit();
	});
	$('#btn-clear-search').click(function(){
		$('input[name="search"]').val('');
		$('#adminListForm').submit();
	});
	
	$('.trash-button').click(function(e)
	{
		e.preventDefault();
		var url = $(this).attr('href');
		$('#btn-confirm-delete').attr('data-url',url);
		$('#deleteModal').modal('show');

	});
	$('#btn-confirm-delete').click(function(){
		var url = $(this).attr('data-url');
		window.location = url;
	});
	$('#btn-check-all').click(function(){
		var check = $(this).is(':checked');
		$('.ckb').each(function(){
			this.checked = check;
		});
	});
	$('#cmb-sort-fields,#cmb-sort-dir').change(function () 
	{
			$('#adminListForm input[name="sortDir"]').val($('#cmb-sort-dir').val());
			$('#adminListForm input[name="sortField"]').val($('#cmb-sort-fields').val());
			$('#adminListForm').submit();
		
	});
	$('.col-sort').click(function(){
		
		var sortField = $(this).attr('data-field');
		var sortDir   = 'ASC';
		
		var existSortDir = $('#adminListForm input[name="sortDir"]').val();

		var existSortField  = $('#adminListForm input[name="sortField"]').val();
		if(existSortDir.toUpperCase()=='ASC' && sortField.toLowerCase() == existSortField.toLowerCase())
		{
			sortDir = 'DESC';
		}
		
		
		$('#adminListForm input[name="sortDir"]').val(sortDir);
		$('#adminListForm input[name="sortField"]').val(sortField);
		$('#adminListForm').submit();

	});
	$('.btn-edit-menu').click(function(){
		var totalCheck = 0;
		
		$('.ckb').each(function(){
			if($(this).is(':checked'))
			{
				totalCheck++;
				var parent = $(this).parent().parent();
				
				parent.find('.td-action').each(function(){
					
					 $(this).find('.edit-button').each(function(){
					 	window.location = $(this).attr('href');
					 });
					
				})
				return;

			}

		});	
		
		if(totalCheck==0)
		{
			$('#notCheckModal').modal('show');
		}
	});
	$('.btn-delete-list-menu').click(function(){
		var totalCheck = 0;
		$('.ckb').each(function(){
			if($(this).is(':checked'))
			{
				totalCheck++;
			}
		});
		if(totalCheck ==0)
		{
			$('#notCheckModal').modal('show');
			return;
		}
		$('#deleteListModal').modal('show');
	});
	$('#btn-confirm-delete-list').click(function(){
		var url = $('#hidden_delete_list').val();
		$('#adminListForm').attr('action',url);
		$('#adminListForm').submit();
	});
	$('.btn-activate-all').click(function(){
		var url = $('#hidden_active_list').val();
		var totalCheck = 0;		
		$('.ckb').each(function(){
			if($(this).is(':checked'))
			{
				totalCheck++;
				$('#adminListForm').attr('action',url);
				$('#adminListForm').submit();

			}

		});	
		
		if(totalCheck==0)
		{
			$('#notCheckModal').modal('show');
		}
		
	});
	$('.btn-deactivate-all').click(function(){
		var url = $('#hidden_deactive_list').val();
		var totalCheck = 0;
		$('.ckb').each(function(){
			if($(this).is(':checked'))
			{
				totalCheck++;
				$('#adminListForm').attr('action',url);
				$('#adminListForm').submit();
			}
		});
		if(totalCheck ==0)
		{
			$('#notCheckModal').modal('show');
			return;
		}
		
		
	});
	var length = $('#adminForm').length;
	if(length)
	{
		$('#adminForm').parsley({
	   	  successClass: "",
	      errorClass: "",
	      classHandler: function (el) {
	       /*console.log(el.$element.closest('.form-group'));*/
	          return el.$element.closest(".form-group");
	      },
	      errorsContainer: function (el) {
	          return el.$element.closest(".form-group");
	      },
	      errorsWrapper: "<span class='help-block'></span>",
	      errorTemplate: "<span></span>",
	      trigger : "change",
	  	}).subscribe('parsley:form:validate', function (formInstance) {
	  		validateFormCallBack(formInstance);
	  	});

	}
  	$('#record_number').on('change',function(){
  		$('#adminListForm').submit();
  	});
  	var loader = null;
  	var div_wrapper = null;
  	var data_hidden = null;
  	var sender 		= null;
  	var success_callback = null;
  	var clone_parent =null;
  	$('.btn-browse').click(function(){
  		var form = $(this).attr('data-form');
  		 loader = $(this).find('img');
  		$('#'+form).find('input[type="file"]').click();
  		$('#'+form).find('.hidden-constraint').remove();
  		var constraint = $(this).attr('data-constraint');
  		if(constraint)
  		{
  			var arrConstrain = constraint.split("|");
  			for (var i = 0; i < arrConstrain.length; i++) 
  			{
  				var conItem  =	arrConstrain[i];
  				var conObj 	 = conItem.split(":");
  				if(conObj.length>1)
  				{
  					var conName = conObj[0];
  					var conVal  = conObj[1];
  					var str = '<input type="hidden" class="hidden-constraint" '
  								+' name="constraint['+conName+']" '+
  								'value="'+conVal+'" >';
  					$('#'+form).append(str);
  				}

  			}
  		}

  		div_wrapper = $(this).closest('.img-wrapper');
  		data_hidden = $(this).attr('data-hidden');
  		sender  = this;

  	});

  	$('.btn-add-gallery').on('click',function () 
  	{
  		var form = $(this).attr('data-form');

  		$('#'+form).find('input[type="file"]').click();
  		$('#'+form).find('.hidden-constraint').remove();
  		var constraint = $(this).attr('data-constraint');
  		if(constraint)
  		{
  			var arrConstrain = constraint.split("|");
  			for (var i = 0; i < arrConstrain.length; i++) 
  			{
  				var conItem  =	arrConstrain[i];
  				var conObj 	 = conItem.split(":");
  				if(conObj.length>1)
  				{
  					var conName = conObj[0];
  					var conVal  = conObj[1];
  					var str = '<input type="hidden" class="hidden-constraint" '
  								+' name="constraint['+conName+']" '+
  								'value="'+conVal+'" >';
  					$('#'+form).append(str);
  				}

  			}
  		}
  		div_wrapper = $(this).closest('.img-wrapper');
  		loader 	= $(div_wrapper).find('.g-loader');
  		box_parent 	= $(div_wrapper).parent();
  		var limit  = parseInt($(box_parent).attr('data-limit'));
  		if(isNaN(limit)) limit = 10;
  		var number =  parseInt($(box_parent).attr('gallery-number'));
  		if(isNaN(number))
  		{
  			number = 0;
  		}
  		number++;
  				
  		data_hidden = $(this).attr('data-hidden');
  		sender  = this;
  		var count = parseInt($(box_parent).attr('rel'));
  		clone_parent = $(div_wrapper).clone(true);
  		if(isNaN(count)) 
  		{
  			count =1;
  		}
  		count++;
  		var that = this;  	
  		loader.call_back = function()
  		{
  			$(that).hide();
  		}
  		success_callback = function()
  		{
  			$(box_parent).attr('gallery-number',number);    	
  			// console.log(number);
  			if(number<limit)
  			{
  				var hidden_id = 'tmp_gallery_'+count;
  				$(box_parent).attr('rel',count);
	  			$(clone_parent).find('input[type="hidden"]').attr('id',hidden_id).val('');
	  			$(clone_parent).find('.btn-add-gallery').attr('data-hidden',hidden_id).attr('rel',count);
	  			$(clone_parent).find('.btn-remove-gallery').attr('data-hidden',hidden_id);
	  			$(box_parent).append(clone_parent);	
	  			var obj = $(clone_parent).find('[rel="tooltip"]').removeAttr('checked');
  			}

  		}
  		
  	});
	$('.btn-remove-gallery').on('click', function(){
		var confirmMsg = confirmMsg || "Are you sure ?";
  		var that = this;
  		bootbox.confirm(confirmMsg, function(result) {
  			if(result)
  			{
  				div_wrapper = $(that).closest('.img-wrapper');
				box_parent 	= $(div_wrapper).parent();
				var clone_parent = $(div_wrapper).clone(true);				
		  		var limit  = parseInt($(box_parent).attr('data-limit'));
		  		var number =  parseInt($(box_parent).attr('gallery-number'));	  		
		  		if(isNaN(number)) number = 0;
		  		$(box_parent).attr('gallery-number',number);
		  		$(div_wrapper).remove();				  		
		  		if(isNaN(limit)) limit = 10;
		  		var rel = parseInt($(box_parent).attr('rel'));
		  		if(isNaN(rel)) rel=0;		  		
		  		if(limit==number)
		  		{
		  			rel++;
		  			var tmp_name = 'tmp_gallery_'+rel;
		  			$(box_parent).attr('rel',rel);		  			
		  			$(clone_parent).find('.btn-add-gallery').show();
		  			$(clone_parent).find('img').remove();
		  			$(clone_parent).find('.btn-add-gallery')
		  					.attr('rel',rel)
		  					.attr('data-hidden',tmp_name)
		  					.before('<div class="img-thumbnail"></div>');
		  			$(clone_parent).find('input[type="hidden"]')
		  							.attr('id','tmp_gallery_'+rel)
		  							.val('');
		  			$(clone_parent).find('.btn-remove-gallery')
		  						   .attr('data-hidden',tmp_name);


					$(box_parent).append(clone_parent);		  			
		  		}
		  		if(number>0)number--;
		  		$(box_parent).attr('gallery-number',number);


  			}
  		});
	})
	$('[data-toggle="popover"]').popover({trigger: 'hover','placement': 'bottom',html: true,});
  	$('.file-to-upload').change(function(e){

  		$(this).parent().submit();
  	});
  	$('.btn-remove-image').click(function(){
  		
  		var confirmMsg = confirmMsg || 'Are you sure ?';
  		var that = this;
  		bootbox.confirm(confirmMsg, function(result) {
		   if(result)
		   {
		   		var count = $(that).parent().find('img.img-thumbnail').length;
		   		if(count)
		   		{
		   			$(that).parent().find('img.img-thumbnail').remove();

		   			
		   			var imgDiv = $('<div class="img-thumbnail thumb"><h3>'+label("strNoImage","No Image")+'</h3></div>');
		   			$(that).parent().find('.btn-browse').before(imgDiv);
		   		}
		   		var hidden = $(that).attr('data-hidden');
		   		$('#'+hidden).val('delete');
		   }
		   else
		   {

		   }
		}); 
  	});

  	$('.btn-dialog').click(function(){
  		var options = {
          "backdrop" : "static"
        }
  		$('#modal-dialog').find('.modal-title').html($(this).attr('data-dialog-title'));
  		var url = $(this).attr('data-url');
  		$('#modal-dialog').find('.modal-body').html('<iframe width="100%" id="iframeModal" height="100%" '+
  						' style="width:100%; height:100%; min-height: 450px; padding: 0;" class="col-sm-12" frameborder="0" '+
  						' allowtransparency="true" src="' + url + '"></iframe>');
  		$('#modal-dialog').modal(options);
  		$('#modal-dialog').modal('show');

  	});
  	var lstProperties = {};

  	$('.frm-upload').submit(
  		function(e){

  			e.preventDefault();
  			var properties = {
  					loader : loader,
  					wrapper: div_wrapper, 		
  					hidden : data_hidden,
  					sender : sender,
  					success_callback : success_callback,	

  			};
  			lstProperties[data_hidden] = properties;

  			function showImage(data,prop)
  			{

  				//hidden_back = data.hidden_back;
  				// if(typeof lstProperties[hidden_back] !='undefined')
  				// {
  				// 	properties = lstProperties[hidden_back];
  				// }
  				if(typeof prop !='undefined')
  				{
  					properties = prop;
  				}

  				var wp = $(properties.wrapper);  				
  				var length = wp.find('img.img-thumbnail').length;
  				var sender = properties.sender;
  				if(length ==0)
  				{
  					wp.find('div.img-thumbnail').remove();
  					img = $('<img class="img-thumbnail thumb" src="'+data.url+'" />');
  					$(sender).before(img);  					
  					

  				}
  				else
  				{
  					wp.find('img.img-thumbnail').attr('src',data.url);
  				}
  				$('#'+properties.hidden).val(data.filename);
  				if(typeof properties.success_callback =='function')
  				{
  					properties.success_callback();
  				}
  			}
  			

  			if(properties.loader !=null)
  			{
  				$(properties.loader).show();
  				$(properties.wrapper).addClass('loading');
  				if(typeof properties.loader.call_back=='function') properties.loader.call_back();

  			}

  			function hideLoader(prop)
  			{

  				// if(typeof hidden_back !='undefined')
  				// {
  				// 	if(typeof lstProperties[hidden_back] !='undefined')
  				// 	{
  				// 		properties = lstProperties[hidden_back];
  				// 	}
  				// }
  				if(typeof prop !='undefined')
  				{
  					properties = prop;
  				}
  				if(properties.loader !=null && typeof properties.loader.hide!='undefined')
  				{	
  					$(properties.loader).hide();
  				}
  				$(properties.wrapper).removeClass('loading');
  			}
  			var that = this;
  			


  			var tmpAjax = $(this).ajaxSubmit({
  				beforeSend:function()
  				{
  					this.hidden_back = data_hidden;
  					this.call_back_properties = properties;
  				},
  				
  				success:function(data)
  				{

  					// console.log(this);
  					data = $.parseJSON(data);
  					// console.log(data);
  					// console.log('Success');
  					hideLoader(this.call_back_properties);
  					if(!data.error)
  					{
  						showImage(data,this.call_back_properties);	
  					}
  					else
  					{
  						show_notification(data.msg);
  					}  		
  						


  				},
  				complete:function(response)
  				{  					
  					hideLoader(this.call_back_properties);
  					$(that)[0].reset();

  				},
  				error:function(error)
  				{
  					console.log(error);
  					hideLoader(this.call_back_properties);
  				}

  			});
  			tmpAjax.properties = properties;
  			console.log(tmpAjax);

  		}
  	);
	
	$('.btn-remove-gallery,.btn-add-gallery').mouseenter(function(){
		$(this).show();
	});
	$('.img-gallery').mouseenter(function  () 
	{
		if($(this).hasClass('loading'))
		{
			return;
		}
		$(this).addClass('enter');
		var length = $(this).find('img').length;
		if(length>0)
		{
			$(this).find('.btn-remove-gallery').show();
			$(this).attr('show-btn','.btn-remove-gallery');

		}
		else
		{
			$(this).find('.btn-add-gallery').show();
			//$(this).attr('show-btn','.btn-add-gallery');
		}
	}).mouseleave(
		function  () 
		{
			var that = this;
			setTimeout(function(){
				var show_btn = $(that).attr('show-btn');
				$(that).find(show_btn).hide();
			}, 300);
		}
	);
	$('.datepicker').datepicker({
		 format: 'dd/M/yyyy',
	});
});

