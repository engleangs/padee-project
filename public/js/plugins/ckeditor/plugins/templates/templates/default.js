﻿/*
 Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
*/
CKEDITOR.addTemplates("default",
	{
		imagesPath:CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates")+"templates/images/"),
		templates:
		[
				{
					title:"Strange Template",
					image:"template2.gif",
					description:"A template that defines two colums, each one with a title, and some text.",
					html:'<table cellspacing="0" cellpadding="0" style="width:100%" border="0"><tr><td style="width:50%"><h3>Title 1</h3></td><td></td><td style="width:50%"><h3>Title 2</h3></td></tr><tr><td>Text 1</td><td></td><td>Text 2</td></tr></table><p>More text goes here.</p>'
				},
				{
					title:"Text and Table",
					image:"template3.gif",
					description:"A title with some text and a table.",
					html:'<div style="width: 80%"><h3>Title goes here</h3><table style="width:150px;float: right" cellspacing="0" cellpadding="0" border="1"><caption style="border:solid 1px black"><strong>Table title</strong></caption><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table><p>Type the text here</p></div>'
				},
				{
					title:"Title and Text",
					image:"template1.gif",
					description:"One main image with a title and text that surround the image.",
					html:'<div style="width:100%;float:left;margin:0 auto;"><div style="width: 100%;float: left;"><h3 style="font-size:24px; font-weight:bold;">Company Name : Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h3><p style="font-size:12px; line-height:30px; margin-top:30px; margin-bottom:30px;">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.</p><h3 style="font-size:24px; font-weight:bold;">Vision : Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h3>  <p style="font-size:12px; line-height:30px; margin-top:30px; margin-bottom:30px;">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.</p><h3 style="font-size:24px; font-weight:bold;">Mission : Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h3>  <p style="font-size:12px; line-height:30px; margin-top:30px; margin-bottom:30px;">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.</p><h3 style="font-size:24px; font-weight:bold;">Values : Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h3>  <p style="font-size:12px; line-height:30px; margin-top:30px; margin-bottom:30px;">Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat reprehenderit in voluptate.</p><h3 style="font-size:24px; font-weight:bold;">Services : Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</h3><ul style="float:left;padding:0;margin:0;width:100%;"><li style="line-height:24px; float:left;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</li><li style="line-height:24px; float:left;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</li><li style="line-height:24px; float:left;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</li><li style="line-height:24px; float:left;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</li><li style="line-height:24px; float:left;">Lorem ipsum dolor sit amet, consetetur sadipscing elitr</li></ul></div>'
				}
		]
	});