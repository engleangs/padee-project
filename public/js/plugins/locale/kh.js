// ParsleyConfig definition if not already set
window.ParsleyConfig = window.ParsleyConfig || {};
window.ParsleyConfig.i18n = window.ParsleyConfig.i18n || {};

// Define then the messages
window.ParsleyConfig.i18n.kh = $.extend(window.ParsleyConfig.i18n.kh|| {}, {
  defaultMessage: "តម្លៃនេះមើលទៅហាក់ដូចជាមិនត្រឹមត្រូវ.",
  type: {
    email:        "តម្លៃនេះគួរតែជាអ៊ីមែល.",
    url:          "តម្លៃនេះគូរតែជាតំណរភ្ជាប់.",
    number:       "តម្លៃនេះគួរតែជាលេខ.",
    integer:      "តម្លៃនេះគួតែជាលេខគត់.",
    digits:       "តម្លៃនេះគួរតែជាតូរលេខ.",
    alphanum:     "តម្លៃនេះគូរតែជាអក្សរ​រឺ​លេខ."
  },
  notblank:       "តម្លៃនេះមិនគួរទទេរ.",
  required:       "តម្លៃនេះចាំបាច់ត្រូវបញ្ចូល.",
  pattern:        "តម្លៃនេះហាក់ដូចជាមិនត្រឹមត្រូវ.",
  min:            "តម្លៃនេះគូរតែធំជាងរឺស្មើ %s.",
  max:            "តម្លៃនេះគូរតែតូចជាងរឺស្មើ %s.",
  range:          "តម្លៃនេះគួរតែចន្លោះ %s​​និង %s.",
  minlength:      "តម្លៃនេះខ្លីពេក. វាគូរតែមាន %s តូរអក្សរ​រឺច្រើនជាងនេះ.",
  maxlength:      "តម្លៃនេះវែងពេក. វាគូរតែមាន %s តូរអក្សរឹតិចជាងនេះ.",
  length:         "តម្លៃនេះហាក់ដូចជាមិនត្រឹមត្រូវ​  ប្រវែងវាគួរតែនៅចន្លោ៖ %s និង %s​ តូរអក្សរ.",
  mincheck:       "ចាំបាច់ត្រូវតែជ្រើសរើស %s .",
  maxcheck:       "ចាំបាច់ត្រូវតែជ្រើសរើ %s រឺក៏តិចជាងនេះ.",
  check:          "ចាំបាច់ត្រូវតែជ្រើសរើសនៅចន្លោះពី​ %s និង %s.",
  equalto:        "តម្លៃនេះគូរតែដូចទៅន."
});

// If file is loaded after Parsley main file, auto-load locale
if ('undefined' !== typeof window.ParsleyValidator)
  window.ParsleyValidator.addCatalog('en', window.ParsleyConfig.i18n.en, true);
