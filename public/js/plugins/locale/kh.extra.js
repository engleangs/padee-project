/*
* @Author: user
* @Date:   2014-12-05 10:32:24
* @Last Modified by:   user
* @Last Modified time: 2014-12-05 10:32:49
*/

window.ParsleyConfig = window.ParsleyConfig || {};
window.ParsleyConfig.i18n = window.ParsleyConfig.i18n || {};

window.ParsleyConfig.i18n.kh = $.extend(window.ParsleyConfig.i18n.kh || {}, {
  dateiso:  "តម្លៃបញ្ចូលនេះគួរតែជាកាលបរិច្ឆេត (YYYY-MM-DD).",
  minwords: "តម្លៃបញ្ចូលនេះខ្លីពេក  វាគួរតែមាន %s ពាក្យរឺច្រើនជាងនេះ.",
  maxwords: "តម្លៃបញ្ចូលនេះខ្លីពេក  វាគួរតែមាន %s ពាក្យរឺតិចជាងនេះ.",
  words:    "តម្លៃបញ្ចូលនេះគូតែចន្លោះ %s​ និង %s ពាក្យ.",
  gt:       "តម្លៃបញ្ចូលគូួរតែធំជាងនេះ",
  gte:      "តម្លៃបញ្ចូលគូួរតែធំជាងរឺស្មើនេះ",
  lt:       "តម្លៃបញ្ចូលគូួរតែតូចជាងនេះ.",
  lte:      "តម្លៃបញ្ចូលគូួរតែតូចជាងនេះ​រឺស្មើ."
});
