/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50508
Source Host           : localhost:3306
Source Database       : db_cars

Target Server Type    : MYSQL
Target Server Version : 50508
File Encoding         : 65001

Date: 2014-10-30 09:41:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ads
-- ----------------------------
DROP TABLE IF EXISTS `ads`;
CREATE TABLE `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `image` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `position` varchar(200) DEFAULT NULL,
  `expired_at` datetime DEFAULT NULL,
  `total_click` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '1',
  `url` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ads
-- ----------------------------

-- ----------------------------
-- Table structure for ads_clicks
-- ----------------------------
DROP TABLE IF EXISTS `ads_clicks`;
CREATE TABLE `ads_clicks` (
  `id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `ads_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ads_clicks
-- ----------------------------

-- ----------------------------
-- Table structure for contacts
-- ----------------------------
DROP TABLE IF EXISTS `contacts`;
CREATE TABLE `contacts` (
  `id` int(11) DEFAULT NULL,
  `first_name` varchar(200) DEFAULT NULL,
  `last_name` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone` varchar(200) DEFAULT NULL,
  `vehicle_make` varchar(200) DEFAULT NULL,
  `vehicle_model` varchar(200) DEFAULT NULL,
  `vehicle_year` int(11) DEFAULT NULL,
  `subject` varchar(200) DEFAULT NULL,
  `content` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of contacts
-- ----------------------------

-- ----------------------------
-- Table structure for dealers
-- ----------------------------
DROP TABLE IF EXISTS `dealers`;
CREATE TABLE `dealers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT '',
  `description` text,
  `phone` varchar(200) DEFAULT '',
  `phone2` varchar(200) DEFAULT '',
  `address` varchar(400) DEFAULT '',
  `email` varchar(200) DEFAULT '',
  `email2` varchar(200) DEFAULT '',
  `website` varchar(300) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `is_featured` tinyint(1) DEFAULT '0',
  `longitude` double DEFAULT '0',
  `latitude` double DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `slug` varchar(100) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `total_vehicle` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`,`language_id`),
  UNIQUE KEY `NewIndex1` (`slug`,`language_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dealers
-- ----------------------------
INSERT INTO `dealers` VALUES ('1', '1', 'Engleang Sam1', '', '+85577816127', '', 'Phnom Penh\r\n181', 'engleangs@gmail.com', '', 'http://www.abi-technologies.com', '69428e6b86b4b025698259f405e71597144d471a.png', '0', '0', '104.33389663696', '11.5444387', '0', 'engleang-sam', '1', '2014-10-28 12:15:15', '2014-10-21 08:44:35', '3');
INSERT INTO `dealers` VALUES ('1', '2', 'Engleang Sam', '', '+85577816127', '', 'Phnom Penh\r\n181', 'engleangs@gmail.com', '', 'http://www.abi-technologies.com', '69428e6b86b4b025698259f405e71597144d471a.png', '0', '0', '104.33389663696', '11.5444387', '0', 'engleang-sam', '1', '2014-10-28 12:15:15', '2014-10-21 08:44:35', '3');
INSERT INTO `dealers` VALUES ('2', '1', 'Engleang Sam', '', '2077981200', '', 'Phnom Penh\r\n181', 'engleangs@gmail.com', '', '', 'a106f8f765c8ef223934cf9c694542164ea32941.png', '0', '0', '104.9132848', '11.5444272', '0', '', '1', '2014-10-28 12:15:19', '2014-10-24 07:00:18', '0');
INSERT INTO `dealers` VALUES ('2', '2', 'Engleang Sam', '', '2077981200', '', 'South Street, college station\r\nBowdoin College', 'engleangs@gmail.com', '', '', 'a106f8f765c8ef223934cf9c694542164ea32941.png', '0', '0', '104.9132848', '11.5444272', '0', '', '1', '2014-10-28 12:15:19', '2014-10-24 07:00:18', '0');
INSERT INTO `dealers` VALUES ('3', '1', 'Abi Tech', '', '2077981200', '', 'Phnom Penh\r\n181', 'engleangs@gmail.com', '', 'http://www.abi-technologies.com', '9c2906f19fb2dbf4e145f434955d9144a2aec2c3.png', '0', '1', '104.9132846', '11.5444261', '0', '-1', '1', '2014-10-28 10:36:04', '2014-10-24 07:02:29', '0');
INSERT INTO `dealers` VALUES ('3', '2', 'អេប៊ីអាយ', '', '2077981200', '', '', 'engleangs@gmail.com', '', 'http://www.abi-technologies.com', '9c2906f19fb2dbf4e145f434955d9144a2aec2c3.png', '0', '1', '104.9132846', '11.5444261', '0', '-1', '1', '2014-10-28 10:36:04', '2014-10-24 07:02:29', '0');

-- ----------------------------
-- Table structure for dealer_images
-- ----------------------------
DROP TABLE IF EXISTS `dealer_images`;
CREATE TABLE `dealer_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `dealer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of dealer_images
-- ----------------------------

-- ----------------------------
-- Table structure for languages
-- ----------------------------
DROP TABLE IF EXISTS `languages`;
CREATE TABLE `languages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(300) DEFAULT NULL,
  `flag` varchar(300) DEFAULT NULL,
  `code` char(3) DEFAULT NULL,
  `is_default` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `NewIndex1` (`code`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of languages
-- ----------------------------
INSERT INTO `languages` VALUES ('1', 'English', 'english.png', 'en', '1');
INSERT INTO `languages` VALUES ('2', 'ខ្មែរ', 'cambodia.png', 'kh', '0');

-- ----------------------------
-- Table structure for makes
-- ----------------------------
DROP TABLE IF EXISTS `makes`;
CREATE TABLE `makes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `image` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `state` int(11) DEFAULT '1',
  `total_model` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of makes
-- ----------------------------
INSERT INTO `makes` VALUES ('1', '1', 'Hello', '<p>Welcome to abi-technologies !!!</p>\r\n', 'fdacb9cfa6738b51b7733d6f0fd5c0ed7d911c3b.png', '2014-10-13 03:29:38', '2014-10-13 03:29:38', '1', '0');
INSERT INTO `makes` VALUES ('1', '2', 'សូស្តី', '<p>សូស្តីសូមស្វាគមន៍មកកាន់ក្រុមហ៊ុនអេប៊ីអាយតិចណូឡូជី</p>\r\n', 'fdacb9cfa6738b51b7733d6f0fd5c0ed7d911c3b.png', '2014-10-13 03:29:38', '2014-10-13 03:29:38', '1', '0');

-- ----------------------------
-- Table structure for models
-- ----------------------------
DROP TABLE IF EXISTS `models`;
CREATE TABLE `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `image` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `make_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT '0',
  `total_vehicle` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of models
-- ----------------------------
INSERT INTO `models` VALUES ('1', '1', 'Hello', 'este	', '', '2014-10-02 16:31:11', '2014-10-28 10:20:51', '1', '0', '3');
INSERT INTO `models` VALUES ('1', '2', 'អេងលាង', null, null, null, '2014-10-28 10:20:51', '1', '0', '3');
INSERT INTO `models` VALUES ('2', '1', 'test', 'test', '', '2014-10-02 16:34:01', '2014-10-02 16:34:01', '1', '0', '0');
INSERT INTO `models` VALUES ('2', '2', 'test', 'test\r\n', null, '2014-10-02 16:34:26', '2014-10-02 16:34:26', '1', '0', '0');

-- ----------------------------
-- Table structure for services
-- ----------------------------
DROP TABLE IF EXISTS `services`;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text,
  `phone` varchar(200) DEFAULT NULL,
  `phone2` varchar(200) DEFAULT NULL,
  `address` varchar(400) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `email2` varchar(200) DEFAULT NULL,
  `website` varchar(300) DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `is_featured` tinyint(1) DEFAULT '0',
  `longitude` double DEFAULT '0',
  `latitude` double DEFAULT '0',
  `hits` int(11) DEFAULT '0',
  `slug` varchar(100) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '1',
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `total_image` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`language_id`),
  UNIQUE KEY `NewIndex1` (`slug`,`language_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of services
-- ----------------------------
INSERT INTO `services` VALUES ('1', '1', 'Engleang Sam', '', '', '', 'Phnom Penh\r\n181', '', '', '', '842b49a7f6282a34e15e7d302c3c45df4678469d.png', '0', '0', '0', '0', 'engleang-sam', '1', '2014-10-28 06:42:15', '2014-10-21 10:17:48', '2');
INSERT INTO `services` VALUES ('1', '2', 'Engleang Sam', '', '+85577816127', '', 'Phnom Penh\r\n181', 'engleangs@gmail.com', '', 'www.abi-technologies.com', '49d3f7839b05e55574820f3d0aaee8d44d637f1d.png', '0', '104.9132568', '11.5444234', '0', 'engleang-sam', '1', '2014-10-21 10:17:48', '2014-10-21 10:17:48', '2');
INSERT INTO `services` VALUES ('2', '1', 'Auto Khmer', '', '+85577816127', '', 'South Street, college station\r\nBowdoin College', 'engleangs@gmail.com', '', 'http://www.abi-technologies.com', '', '0', '104.9132363', '11.5444305', '0', 'auto-khmer', '1', '2014-10-28 06:42:15', '2014-10-21 10:35:36', '0');
INSERT INTO `services` VALUES ('2', '2', 'Engleang Sam', '', '', '', 'Phnom Penh\r\n181', '', '', '', '842b49a7f6282a34e15e7d302c3c45df4678469d.png', '0', '0', '0', '0', 'auto-khmer', '1', '2014-10-28 06:42:15', '2014-10-21 10:35:36', '0');
INSERT INTO `services` VALUES ('3', '1', 'Engleang Sam', '', '+85577816127', '', 'Phnom Penh\r\n181', 'engleangs@gmail.com', '', 'http://www.abi-technologies.com', '5868d4fd06d96f78d5c046ccf3205447744402ac.png', '1', '104.9133092', '11.5444146', '0', 'engleang-sam-1', '1', '2014-10-28 05:03:55', '2014-10-28 05:03:55', '0');
INSERT INTO `services` VALUES ('3', '2', 'Engleang Sam', '', '+85577816127', '', 'Phnom Penh\r\n181', 'engleangs@gmail.com', '', 'http://www.abi-technologies.com', '5868d4fd06d96f78d5c046ccf3205447744402ac.png', '1', '104.9133092', '11.5444146', '0', 'engleang-sam-1', '1', '2014-10-28 05:03:55', '2014-10-28 05:03:55', '0');
INSERT INTO `services` VALUES ('4', '1', 'Engleang Sam', '', '', '', 'Phnom Penh\r\n181', '', '', '', '18542fdb2fd213c7be46ea8fea06bc9879f649ae.png', '0', '0', '0', '0', 'engleang-sam-2', '1', '2014-10-28 06:50:41', '2014-10-28 05:04:26', '0');
INSERT INTO `services` VALUES ('4', '2', 'Engleang Sam', '', '', '', 'Phnom Penh\r\n181', '', '', '', '18542fdb2fd213c7be46ea8fea06bc9879f649ae.png', '0', '0', '0', '0', 'engleang-sam-2', '1', '2014-10-28 06:50:41', '2014-10-28 05:04:26', '0');

-- ----------------------------
-- Table structure for service_images
-- ----------------------------
DROP TABLE IF EXISTS `service_images`;
CREATE TABLE `service_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `service_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of service_images
-- ----------------------------
INSERT INTO `service_images` VALUES ('1', '1e144a244b08d0f4baad5e8906253012533a1dea.png', '2014-10-21 10:17:48', '2014-10-21 10:17:48', '1');
INSERT INTO `service_images` VALUES ('2', '630add4d9e720847b8655c3b300811ea0e200732.png', '2014-10-21 10:17:48', '2014-10-21 10:17:48', '1');
INSERT INTO `service_images` VALUES ('3', '', '2014-10-21 10:35:36', '2014-10-28 06:24:40', '2');

-- ----------------------------
-- Table structure for tmp_slug
-- ----------------------------
DROP TABLE IF EXISTS `tmp_slug`;
CREATE TABLE `tmp_slug` (
  `slug` varchar(300) DEFAULT NULL,
  `serial` bigint(67) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tmp_slug
-- ----------------------------

-- ----------------------------
-- Table structure for transmissions
-- ----------------------------
DROP TABLE IF EXISTS `transmissions`;
CREATE TABLE `transmissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL DEFAULT '1',
  `name` varchar(300) DEFAULT '',
  PRIMARY KEY (`id`,`language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of transmissions
-- ----------------------------

-- ----------------------------
-- Table structure for trims
-- ----------------------------
DROP TABLE IF EXISTS `trims`;
CREATE TABLE `trims` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `description` text,
  `image` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `total_vehicle` int(11) DEFAULT '0',
  PRIMARY KEY (`id`,`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of trims
-- ----------------------------
INSERT INTO `trims` VALUES ('1', '1', 'test1', '<p>ddd</p>\r\n', '', '2014-10-24 04:40:56', '2014-10-28 10:20:51', '1', null);
INSERT INTO `trims` VALUES ('1', '2', 'test1', '', '', '2014-10-24 04:40:56', '2014-10-28 10:20:51', '1', null);
INSERT INTO `trims` VALUES ('2', '1', 'teest2', '', '', '2014-10-24 04:41:18', '2014-10-24 04:41:18', '1', null);
INSERT INTO `trims` VALUES ('2', '2', 'test2', '<p>dddd</p>\r\n', '', '2014-10-24 04:41:18', '2014-10-24 04:41:18', '1', null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT '0',
  `user_role` varchar(255) DEFAULT NULL,
  `state` int(11) DEFAULT '1',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `remember_token` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('3', 'Chay Rambo 168', 'chhayrambo.sim@abi-technologies.com', 'rambo', '$2y$10$nD0lnRlKgTFP3KRbYITYguB1IQykCMMiKOesmnXWEl2fpUVzkPuLu', '0', '1', '1', '2014-08-25 08:18:19', '2014-10-08 09:27:16', null, null, '2EGo5F6CDNJkJ1KjsYsdfdgr5mixXbT2KksjYeSgFf15XQi6fhmImv3T0fbk');
INSERT INTO `users` VALUES ('4', 'Engleang Sam', 'engleangs@gmail.com', 'engleangs', '$2y$10$gqp.6OMqZXRSYg0Z1ap8m.wfsoypeHsK7TWHcZ2/PAo3WuDzYkgcm', '0', '1', '1', '2014-08-26 02:52:37', '2014-10-08 09:26:42', null, null, '234AD4OOpXE28Si68oEqw7srVsKeoy7TpCJ0OR54a8H5qjquj7IcEFuzaZlL');
INSERT INTO `users` VALUES ('5', 'Sreyleak Hang', 'hang.sreyleak099@gmail.com', 'sreyleak099', '$2y$10$619OBDW8sYsziDri/eh6XegAzJE2uCpLP8pS7cJzV.dWSrcNZtOfW', '0', '1', '1', '2014-08-27 07:23:44', '2014-10-08 09:26:42', null, null, 'Aq0TmXH3ZDYkkF1h8WiCVdztYc8QS959Oij4jbgGe9g1nAgqrHJLMlmnLH76');

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('1', 'Admin', '2014-10-08 09:24:34', '2014-10-08 09:24:34');
INSERT INTO `user_roles` VALUES ('2', 'User', '2014-10-08 09:24:40', '2014-10-08 09:24:40');

-- ----------------------------
-- Table structure for vehicles
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `description` text,
  `mileague` double DEFAULT NULL,
  `type` int(11) DEFAULT '1',
  `trim_id` int(11) DEFAULT NULL,
  `cylinder` varchar(200) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `exterior_color` varchar(200) DEFAULT NULL,
  `interior_color` varchar(200) DEFAULT NULL,
  `fuel_type` varchar(200) DEFAULT NULL,
  `transmission` varchar(200) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `is_featured` tinyint(1) DEFAULT '0',
  `dealer_id` int(11) DEFAULT NULL,
  `make_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `hits` int(11) DEFAULT '0',
  `slug` varchar(300) DEFAULT NULL,
  `total_compared` int(11) DEFAULT '0',
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `feature_image` varchar(200) DEFAULT NULL,
  `state` tinyint(4) DEFAULT '1',
  `total_image` int(11) DEFAULT NULL,
  `engine` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`,`language_id`),
  FULLTEXT KEY `NewIndex1` (`description`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vehicles
-- ----------------------------
INSERT INTO `vehicles` VALUES ('3', '2', 'Camry3201', '<p>ddd</p>\r\n', '333', '0', '1', '1000', '2000', 'បៃតង2', 'បៃតង', 'សំាង', 'Auto3', '123', '1', '1', null, '1', '2014-10-28 09:56:27', '2014-10-28 09:56:27', '0', 'camry3101', '0', '11.5444513', '104.9133261', 'a36862c1b85c8d88440eb992748b76ff48440fec.png', '1', '0', null);
INSERT INTO `vehicles` VALUES ('3', '1', 'Camry3101', '<p>ddd</p>\r\n', '333', '0', '1', '1000', '2000', 'Green', 'Green', 'Gasoline', 'Auto3', '123', '1', '1', null, '1', '2014-10-28 09:56:27', '2014-10-28 09:56:27', '0', 'camry3101', '0', '11.5444513', '104.9133261', 'a36862c1b85c8d88440eb992748b76ff48440fec.png', '1', '0', null);
INSERT INTO `vehicles` VALUES ('2', '1', 'Lexus330 Model 30303', '', '13', '1', '1', '10', '20', 'Silver', 'Silver', 'Gasoline', 'Auto', '123', '1', '1', null, '1', '2014-10-28 07:52:11', '2014-10-28 08:32:09', '0', 'lexus330-model-30303', '0', '11.5444513', '104.9133261', '839bb415d69168facc883a3b77e15d772c6dfa65.png', '1', '1', null);
INSERT INTO `vehicles` VALUES ('2', '2', 'Lexus330 Model 30303', '', '13', '1', '1', '10', '20', 'Silver', 'Silver', 'សំាង', 'Auto', '123', '1', '1', null, '1', '2014-10-28 07:52:11', '2014-10-28 08:32:09', '0', 'lexus330-model-30303', '0', '11.5444513', '104.9133261', '839bb415d69168facc883a3b77e15d772c6dfa65.png', '1', '1', null);

-- ----------------------------
-- Table structure for vehicle_images
-- ----------------------------
DROP TABLE IF EXISTS `vehicle_images`;
CREATE TABLE `vehicle_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(200) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of vehicle_images
-- ----------------------------
INSERT INTO `vehicle_images` VALUES ('1', '17ac047449ba3b41259ff2cd338e3a6b24c1dc97.png', '2014-10-24 07:43:28', '2014-10-28 07:52:11', '2');
INSERT INTO `vehicle_images` VALUES ('2', '1a3ba2f1ad1f1cce9e5a2f2a38f27af5e5a8a976.png', '2014-10-28 02:32:36', '2014-10-28 03:00:11', '0');
INSERT INTO `vehicle_images` VALUES ('3', '', '2014-10-28 03:38:22', '2014-10-28 09:56:27', '3');
INSERT INTO `vehicle_images` VALUES ('4', '', '2014-10-28 03:38:22', '2014-10-28 09:56:27', '3');
INSERT INTO `vehicle_images` VALUES ('5', '', '2014-10-28 03:46:32', '2014-10-28 09:56:27', '3');
INSERT INTO `vehicle_images` VALUES ('6', '', '2014-10-28 04:01:03', '2014-10-28 04:15:50', '3');
INSERT INTO `vehicle_images` VALUES ('7', '', '2014-10-28 04:02:05', '2014-10-28 04:15:41', '3');
INSERT INTO `vehicle_images` VALUES ('8', '', '2014-10-28 04:07:31', '2014-10-28 04:08:48', '3');
INSERT INTO `vehicle_images` VALUES ('9', 'a5bbef461d838482e14ba0be3bc8f55a593fe657.png', '2014-10-28 04:39:34', '2014-10-28 07:46:57', '1');
INSERT INTO `vehicle_images` VALUES ('10', '', '2014-10-28 04:39:34', '2014-10-28 07:46:57', '1');
INSERT INTO `vehicle_images` VALUES ('11', '', '2014-10-28 04:39:34', '2014-10-28 07:46:57', '1');
INSERT INTO `vehicle_images` VALUES ('12', '', '2014-10-28 04:41:28', '2014-10-28 07:46:57', '1');
INSERT INTO `vehicle_images` VALUES ('13', '', '2014-10-28 04:41:56', '2014-10-28 07:46:57', '1');
INSERT INTO `vehicle_images` VALUES ('14', '8abc3b67868498abad90a202ec0956f53e443112.png', '2014-10-28 06:53:13', '2014-10-28 06:53:13', '7');
